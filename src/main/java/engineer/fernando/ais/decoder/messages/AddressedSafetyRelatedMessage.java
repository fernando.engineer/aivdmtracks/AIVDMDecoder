/*
 * AddressedSafetyRelatedMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

import java.util.Objects;

@SuppressWarnings("serial")
public class AddressedSafetyRelatedMessage extends AISMessage {

    private transient Integer sequenceNumber;
    private transient MMSI destinationMmsi;
    private transient Boolean retransmit;
    private transient Integer spare;
    private transient String text;

    public AddressedSafetyRelatedMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public AddressedSafetyRelatedMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressedSafetyRelatedMessage)) return false;
        if (!super.equals(o)) return false;
        AddressedSafetyRelatedMessage that = (AddressedSafetyRelatedMessage) o;
        return getSequenceNumber().equals(that.getSequenceNumber()) && getDestinationMmsi().equals(that.getDestinationMmsi())
                && getRetransmit().equals(that.getRetransmit()) && getSpare().equals(that.getSpare()) && getText().equals(that.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSequenceNumber(), getDestinationMmsi(), getRetransmit(), getSpare(), getText());
    }

    public AISMessageType getMessageType() {
        return AISMessageType.ADDRESSED_SAFETY_RELATED_MESSAGE;
    }

    @SuppressWarnings("unused")
	public Integer getSequenceNumber() {
        return getDecodedValue(() -> sequenceNumber, value -> sequenceNumber=value,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

    @SuppressWarnings("unused")
	public MMSI getDestinationMmsi() {
        return getDecodedValue(() -> destinationMmsi, value -> destinationMmsi = value,
                () -> Boolean.TRUE, () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

    @SuppressWarnings("unused")
	public Boolean getRetransmit() {
        return getDecodedValue(() -> retransmit, value -> retransmit = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(70, 71)));
	}

    @SuppressWarnings("unused")
	public Integer getSpare() {
        return getDecodedValue(() -> spare, value -> spare = value, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(71, 72)));
	}

    @SuppressWarnings("unused")
	public String getText() {
        return getDecodedValue(() -> text, value -> text = value, () -> Boolean.TRUE, () -> {
            int extraBitsOfChars = ((getNumberOfBits() - 72)/6)*6;
            return Decoders.STRING_DECODER.apply(getBits(72, 72 + extraBitsOfChars));
        });
	}

    @Override
    public String toString() {
        return "AddressedSafetyRelatedMessage{" +
                "messageType=" + getMessageType() +
                ", sequenceNumber=" + getSequenceNumber() +
                ", destinationMmsi=" + getDestinationMmsi() +
                ", retransmit=" + getRetransmit() +
                ", spare=" + getSpare() +
                ", text='" + getText() + '\'' +
                "} " + super.toString();
    }

}