/*
 * AddressedBinaryMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.specific.ApplicationSpecificMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

import java.lang.ref.WeakReference;
import java.util.Objects;

@SuppressWarnings("serial")
public class AddressedBinaryMessage extends AISMessage {

    private transient Integer sequenceNumber;
    private transient MMSI destinationMmsi;
    private transient Boolean retransmit;
    private transient Integer spare;
    private transient Integer designatedAreaCode;
    private transient Integer functionalId;
    private transient WeakReference<String> binaryData;
    private transient WeakReference<ApplicationSpecificMessage> applicationSpecificMessage;

    public AddressedBinaryMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public AddressedBinaryMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressedBinaryMessage)) return false;
        if (!super.equals(o)) return false;
        AddressedBinaryMessage that = (AddressedBinaryMessage) o;
        return getSequenceNumber().equals(that.getSequenceNumber()) && getDestinationMmsi().equals(that.getDestinationMmsi())
                && getRetransmit().equals(that.getRetransmit()) && Objects.equals(getSpare(), that.getSpare()) &&
                getDesignatedAreaCode().equals(that.getDesignatedAreaCode()) && getFunctionalId().equals(that.getFunctionalId()) &&
                getBinaryData().equals(that.getBinaryData()) && getApplicationSpecificMessage().equals(that.getApplicationSpecificMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSequenceNumber(), getDestinationMmsi(), getRetransmit(), getSpare(),
                getDesignatedAreaCode(), getFunctionalId(), getBinaryData(), getApplicationSpecificMessage());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.ADDRESSED_BINARY_MESSAGE;
    }

    @SuppressWarnings("unused")
    public Integer getSequenceNumber() {
        return getDecodedValue(() -> sequenceNumber, ref -> sequenceNumber = ref, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

    @SuppressWarnings("unused")
	public MMSI getDestinationMmsi() {
        return getDecodedValue(() -> destinationMmsi, ref -> destinationMmsi = ref, () -> Boolean.TRUE,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

    @SuppressWarnings("unused")
	public Boolean getRetransmit() {
        return getDecodedValue(() -> retransmit, ref -> retransmit = ref, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(70, 71)));
	}

    @SuppressWarnings("unused")
	public int getSpare() {
        return getDecodedValue(() -> spare, ref -> spare = ref, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(71, 72)));
	}

    @SuppressWarnings("unused")
	public Integer getDesignatedAreaCode() {
        return getDecodedValue(() -> designatedAreaCode, ref -> designatedAreaCode = ref,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(72, 82)));
	}

    @SuppressWarnings("unused")
	public Integer getFunctionalId() {
        return getDecodedValue(() -> functionalId, ref -> functionalId = ref,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(82, 88)));
	}

    @SuppressWarnings("unused")
	public String getBinaryData() {
        return getDecodedValueByWeakReference(() -> binaryData, ref -> binaryData = ref, () -> Boolean.TRUE, () -> Decoders.BIT_DECODER.apply(getBits(88, getNumberOfBits())));
	}

    @SuppressWarnings("unused")
    public ApplicationSpecificMessage getApplicationSpecificMessage() {
        ApplicationSpecificMessage asm = this.applicationSpecificMessage == null ? null : this.applicationSpecificMessage.get();
        if (asm == null) {
            asm = ApplicationSpecificMessage.create(getDesignatedAreaCode(), getFunctionalId(), getBinaryData());
            applicationSpecificMessage = new WeakReference<>(asm);
        }

        if (asm.getDesignatedAreaCode() >= 0 && asm.getDesignatedAreaCode() != this.getDesignatedAreaCode().intValue())
            throw new IllegalStateException("Implementation error: DAC of AISMessage does not match ASM: " + asm.getDesignatedAreaCode() + " " + this.getDesignatedAreaCode());

        if (asm.getFunctionalId() >= 0 && asm.getFunctionalId() != this.getFunctionalId().intValue())
            throw new IllegalStateException("Implementation error: FI of AISMessage does not match ASM: " + asm.getFunctionalId() + " " + this.getFunctionalId());

        return asm;
    }

    @Override
    public String toString() {
        return "AddressedBinaryMessage{" +
                "messageType=" + getMessageType() +
                ", sequenceNumber=" + getSequenceNumber() +
                ", destinationMmsi=" + getDestinationMmsi() +
                ", retransmit=" + getRetransmit() +
                ", spare=" + getSpare() +
                ", designatedAreaCode=" + getDesignatedAreaCode() +
                ", functionalId=" + getFunctionalId() +
                ", binaryData='" + getBinaryData() + '\'' +
                "} " + super.toString();
    }

}