/*
 * PositionReportClassAScheduled
 */


package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;


@SuppressWarnings("serial")
public class PositionReportClassAScheduled extends PositionReport {

    public PositionReportClassAScheduled(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public String toString() {
        return "PositionReportClassAScheduled{" +
                "messageType=" + getMessageType() +
                "} " + super.toString();
    }

    @Override
    public AISMessageType getMessageType() {
        return AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED;
    }
}