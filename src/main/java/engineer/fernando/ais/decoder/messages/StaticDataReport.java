/*
 * StaticDataReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.ShipType;

public interface StaticDataReport extends DataReport {

	String getCallsign();
	String getShipName();
	ShipType getShipType();
	Integer getToBow();
	Integer getToStern();
	Integer getToStarboard();
	Integer getToPort();

}