/*
 * ReportingInterval
 */

package engineer.fernando.ais.decoder.messages.types;

public enum ReportingInterval {
	AUTONOMOUS(0),
	TEN_MINUTES(1),
	SIX_MINUTES(2),
	THREE_MINUTES(3),
	ONE_MINUTE(4),
	THIRTY_SECONDS(5),
	FIFTEEN_SECONDS(6),
	TEN_SECONDS(7),
	FIVE_SECONDS(8),
	NEXT_SHORT_REPORTING_INTERVAL(9),
	NEXT_LONGER_REPORTING_INTERVAL(10),
	FUTURE_USE_1(11),
	FUTURE_USE_2(12),
	FUTURE_USE_3(13),
	FUTURE_USE_4(14),
	FUTURE_USE_5(15);

	ReportingInterval(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static ReportingInterval fromInteger(Integer integer) {
		if (integer != null) {
			for (ReportingInterval b : ReportingInterval.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}