package engineer.fernando.ais.decoder.messages.specific;

public final class UnknownApplicationSpecificMessage extends ApplicationSpecificMessage {

    UnknownApplicationSpecificMessage(int designatedAreaCode, int functionalId, String binaryData) {
        super(designatedAreaCode, functionalId, binaryData);
    }

}