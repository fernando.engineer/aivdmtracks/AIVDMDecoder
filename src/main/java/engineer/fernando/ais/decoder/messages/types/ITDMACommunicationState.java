/*
 * ITDMACommunicationState
 */

package engineer.fernando.ais.decoder.messages.types;

import java.io.Serializable;

import static engineer.fernando.ais.decoder.util.Decoders.BOOLEAN_DECODER;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;
import static java.util.Objects.requireNonNull;

public class ITDMACommunicationState extends CommunicationState implements Serializable {

	private final Integer slotIncrement;
	private final Integer numberOfSlots;
	private final Boolean keepFlag;

	private ITDMACommunicationState(SyncState syncState, Integer slotIncrement, Integer numberOfSlots, Boolean keepFlag) {
		super(syncState);
		this.slotIncrement = slotIncrement;
		this.numberOfSlots = numberOfSlots;
		this.keepFlag = keepFlag;
	}

	public static ITDMACommunicationState fromBitString(String bitString) {
		requireNonNull(bitString);
		bitString = bitString.trim();

		if (bitString.length() != 19 || !bitString.matches("([01])*")){
			return null;
		}

		return new ITDMACommunicationState(
			SyncState.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(0, 2))),
			UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(2, 15)),
			UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(15, 18)),
			BOOLEAN_DECODER.test(bitString.substring(18, 19))
		);
	}

	@SuppressWarnings("unused")
	public Integer getSlotIncrement() {
		return slotIncrement;
	}

	@SuppressWarnings("unused")
	public Integer getNumberOfSlots() {
		return numberOfSlots;
	}

	@SuppressWarnings("unused")
	public Boolean getKeepFlag() {
		return keepFlag;
	}

	@Override
	public String toString() {
		return "ITDMACommunicationState{" +
				super.toString() +
				", slotIncrement=" + slotIncrement +
				", numberOfSlots=" + numberOfSlots +
				", keepFlag=" + keepFlag +
				"}";
	}
}