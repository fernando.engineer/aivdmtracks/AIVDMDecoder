/*
 * ManeuverIndicator
 */

package engineer.fernando.ais.decoder.messages.types;

public enum ManeuverIndicator {
	NOT_AVAILABLE(0),
	NO_SPECIAL_MANEUVER(1),
	SPECIAL_MANEUVER(2);

	ManeuverIndicator(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static ManeuverIndicator fromInteger(Integer integer) {
		if (integer != null) {
			for (ManeuverIndicator b : ManeuverIndicator.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}