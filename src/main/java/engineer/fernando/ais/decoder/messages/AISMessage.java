/*
 * AISMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.exceptions.InvalidMessage;
import engineer.fernando.ais.decoder.exceptions.UnsupportedNMEAMessageType;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.messages.types.NMEATagBlock;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;
import engineer.fernando.ais.decoder.util.LoggerTools;
import engineer.fernando.ais.decoder.util.StringTools;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.function.BiFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;



public abstract class AISMessage implements Serializable, CachedDecodedValues {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(AISMessage.class);

    static {
        LoggerTools.logInfo(LOGGER, StringTools.version());
    }

    // The NMEA messages which represent this AIS message
    private NMEAMessage[] nmeaMessages;
    private Metadata metadata;


    // Payload expanded to string of 0's and 1's.
    private transient String bitString = null;

    //Length of bitString
    private transient int numberOfBits = -1;

    private transient Integer repeatIndicator;
    private transient MMSI sourceMmsi;
    private transient NMEATagBlock nmeaTagBlock;

    protected AISMessage() {
    }

    protected AISMessage(NMEAMessage[] nmeaMessages) {
        requireNonNull(nmeaMessages);
        this.nmeaMessages = nmeaMessages;
        checkAISMessageType();
    }

    protected AISMessage(NMEAMessage[] nmeaMessages, String bitString) {
        this(nmeaMessages);
        this.bitString = bitString;

    }



    /**
     * Checks the bit message length for the AIS data payload contained in the NMEA sentence(s) according its message type.
     * @throws InvalidMessage if the AIS payload of the NMEAmessage(s) is invalid.
     */
    protected void checkAISMessageType() throws UnsupportedNMEAMessageType, InvalidMessage{
        AISMessageType nmeaMessageType = decodeMessageType();
        if (getMessageType() != nmeaMessageType) {
            throw new UnsupportedNMEAMessageType(nmeaMessageType.getCode());
        }
        var errorMessage = new StringBuilder();
        final var localbitString = getBitString();
        if (localbitString.length() < Decoders.MIN_MSG_LEN){
            errorMessage.append(StringTools.tooShortToDetermine(localbitString));
        }
        final var messageType = Integer.parseInt(localbitString.substring(0, 6), 2);

        if (messageType < AISMessageType.MIN_CODE || messageType > AISMessageType.MAX_CODE){
            errorMessage.append(StringTools.unsupportedMessage(messageType));
        }

        errorMessage.append(AISMessageType.checkNumberOfBits(nmeaMessageType, getNumberOfBits()));

        if (errorMessage.length() > 0) {
            if (numberOfBits >= 38){
                errorMessage.append(format(" Assumed sourceMmsi: %d.", getSourceMmsi().getMMSI()));
            }
            throw new InvalidMessage(errorMessage.toString());
        }
    }

    public NMEAMessage[] getNmeaMessages() {
        return nmeaMessages;
    }

    public abstract AISMessageType getMessageType();

    public final Metadata getMetadata() {
        return metadata;
    }

    public final void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public final void setTagBlock(NMEATagBlock nmeaTagBlock) {
        this.nmeaTagBlock = nmeaTagBlock;
    }


    private AISMessageType decodeMessageType() {
        return AISMessageType.fromInteger(Integer.parseInt(getBits(0, 6), 2));
    }

    public final Integer getRepeatIndicator() {
        return getDecodedValue(() -> repeatIndicator, value -> repeatIndicator = value, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(6, 8)));
    }

    public final MMSI getSourceMmsi() {
        return getDecodedValue(() -> sourceMmsi, value -> sourceMmsi = value, () -> Boolean.TRUE,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(8, 38))));
    }

    @Override
    public String toString() {
        return "AISMessage{" +
                "nmeaMessages=" + Arrays.toString(nmeaMessages) +
                ", metadata=" + metadata +
                ", repeatIndicator=" + getRepeatIndicator() +
                ", sourceMmsi=" + getSourceMmsi() +
                '}' + tagBlockToString();
    }

    public String tagBlockToString() {
        String tagBlockMessage;
        if (nmeaTagBlock != null) {
            tagBlockMessage = String.valueOf(nmeaTagBlock);

        } else {
            tagBlockMessage = "";
        }
        return tagBlockMessage;
    }

    protected String getBitString() {
        var b = bitString;
        if (b == null) {
            b = decodePayloadToBitString(nmeaMessages);
            bitString = b;
        }
        return b;
    }

    protected String getZeroBitStuffedString(int endIndex) {
        var b = getBitString();
        if (b.length() - endIndex < 0) {
            var c = new StringBuilder(b);
            for (int i = b.length() - endIndex; i < 0; i++) {
                c = c.append("0");
            }
            b = c.toString();
        }
        return b;
    }

    public String getBits(int beginIndex, int endIndex) {
        return getZeroBitStuffedString(endIndex).substring(beginIndex, endIndex);
    }

    protected int getNumberOfBits() {
        if (numberOfBits < 0) {
            numberOfBits = getBitString().length();
        }
        return numberOfBits;
    }

    protected static String decodePayloadToBitString(NMEAMessage... nmeaMessages) {
        var sixBitEncodedPayload = new StringBuilder();
        int fillBits = -1;
        for (var i = 0; i < nmeaMessages.length; i++) {
            NMEAMessage m = nmeaMessages[i];
            sixBitEncodedPayload.append(m.getEncodedPayload());
            if (i == nmeaMessages.length - 1) {
                fillBits = m.getFillBits();
            }
        }

        // The AIS message payload stored as a string of 0's and 1's
        return toBitString(sixBitEncodedPayload.toString(), fillBits);
    }


    /**
     * Create proper type of AISMessage from 1..n NMEA messages, and
     * attach metadata.
     *
     * @param metadata     Meta data
     * @param nmeaTagBlock NMEA Tag Block
     * @param nmeaMessages NMEA messages
     * @return AISMessage the AIS message
     * @throws InvalidMessage if the AIS payload of the NMEAmessage(s) is invalid
     */
    public static AISMessage create(Metadata metadata, NMEATagBlock nmeaTagBlock, NMEAMessage... nmeaMessages) {
        var aisMessage = create(nmeaMessages);
        aisMessage.setTagBlock(nmeaTagBlock);
        aisMessage.setMetadata(metadata);
        return aisMessage;
    }

    /**
     * Create proper type of AISMessage from 1..n NMEA messages.
     *
     * @param nmeaMessages NMEA messages
     * @return AISMessage the AIS message
     * @throws InvalidMessage if the AIS payload of the NMEAmessage(s) is invalid
     */
    public static AISMessage create(NMEAMessage... nmeaMessages) {
        BiFunction<NMEAMessage[], String, AISMessage> aisMessageConstructor = null;
        var bitString = decodePayloadToBitString(nmeaMessages);
        var messageType = AISMessageType.fromInteger(Integer.parseInt(bitString.substring(0, 6), 2));
        if (messageType == null) {
            var sb = new StringBuilder();
            for (NMEAMessage nmeaMessage : nmeaMessages) {
                sb.append(nmeaMessage);
            }
            throw new InvalidMessage("Cannot extract message type from NMEA message: " + sb.toString());
        }
        else if(! AISMessageType.LENGTHS.containsKey(messageType.getCode())){
            throw new UnsupportedNMEAMessageType(messageType.getCode());
        }
        else {
            aisMessageConstructor = detectAISMessageClass(messageType);
        }
        if (aisMessageConstructor != null) {
            return aisMessageConstructor.apply(nmeaMessages, bitString);
        }
        else {
            throw new UnsupportedNMEAMessageType(AISMessageType.ERROR.getCode());
        }
    }

    private static BiFunction<NMEAMessage[], String, AISMessage> detectAISMessageClass(AISMessageType messageType){
        try {
            var classLoader = AISMessageType.AIS_MESSAGES_CLASSES.get(messageType.getCode()).getClassLoader();
            Class<AISMessage> clazz = (Class<AISMessage>) classLoader.loadClass(AISMessageType.AIS_MESSAGES_CLASSES.get(messageType.getCode()).getName());
            return (nmeaMessages1, s) -> {
                try {
                    return createDetectedInstance(clazz, nmeaMessages1, s);
                } catch (Exception e) {
                    if(LOGGER.isErrorEnabled()){
                        new StringBuilder().append("Exception: ").append(e).toString();
                    }
                }
                return null;
            };
        } catch (ClassNotFoundException e) {
            LOGGER.error(String.format("Exception: %s", e));
        }
        return null;
    }

    private static AISMessage createDetectedInstance(Class<AISMessage> clazz, NMEAMessage[] nmeaMessages1, String s) throws Exception {
        try {
            Constructor<AISMessage> aisMessageConstructor = clazz.getConstructor(NMEAMessage[].class, String.class);
            return aisMessageConstructor.newInstance(nmeaMessages1, s);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                LOGGER.error(String.format("Exception: %s", e.getCause()));
                throw new Exception(e.toString(), e.getCause());
        }
    }

    public boolean isValid() {
        final var localBitString = getBitString();
        if (localBitString.length() < 6) {
            if(LOGGER.isWarnEnabled()){
                LOGGER.warn(StringTools.bitMessage(StringTools.WARNING_TOO_SHORT, localBitString));
            }
            return Boolean.FALSE;
        }
        var messageType = Integer.parseInt(localBitString.substring(0, 6), 2);
        if (messageType < AISMessageType.MIN_CODE || messageType > AISMessageType.MAX_CODE) {
            if(LOGGER.isWarnEnabled()){
                LOGGER.warn(StringTools.unsupportedMessage(messageType));
            }
            return Boolean.FALSE;
        }
        int actualMessageLength = localBitString.length();
        if (!AISMessageType.LENGTHS.get(messageType).contains(actualMessageLength)) {
            if(LOGGER.isWarnEnabled()){
                LOGGER.warn(StringTools.bitMessageIllegal(messageType,StringTools.ILLEGAL_LENGTH, localBitString));
            }
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    /** Decode an encoded six-bit string into a binary string of 0's and 1's */
    private static String toBitString(String encodedString, Integer paddingBits) {
        var bitString = new StringBuilder();
        int n = encodedString.length();
        for (var i = 0; i < n; i++) {
            var c = encodedString.substring(i, i + 1);
            bitString.append(Decoders.getCharToSixBit().get(c));
        }
        return bitString.substring(0, bitString.length() - paddingBits);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AISMessage)) return false;
        AISMessage that = (AISMessage) o;
        if (!getBitString().equals(that.getBitString())) return false;
        return metadata != null ? metadata.equals(that.metadata) : that.metadata == null;
    }

    @Override
    public int hashCode() {
        int result = metadata != null ? metadata.hashCode() : 0;
        result = 31 * result + getBitString().hashCode();
        return result;
    }
}
