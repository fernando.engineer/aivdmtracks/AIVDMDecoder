package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.NavigationStatus;
import engineer.fernando.ais.decoder.messages.types.TransponderClass;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

@SuppressWarnings("serial")
public class LongRangeBroadcastMessage extends AISMessage implements DynamicDataReport {

    private transient Boolean positionAccuracy;
    private transient Boolean raim;
    private transient NavigationStatus navigationStatus;
    private transient Float longitude;
    private transient Float latitude;
    private transient Integer speed;
    private transient Integer course;
    private transient Integer positionLatency;
    private transient Integer spare;


    public LongRangeBroadcastMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public LongRangeBroadcastMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LongRangeBroadcastMessage)) return false;
        if (!super.equals(o)) return false;
        LongRangeBroadcastMessage that = (LongRangeBroadcastMessage) o;
        return getPositionAccuracy().equals(that.getPositionAccuracy()) && getRaim().equals(that.getRaim()) && navigationStatus == that.navigationStatus
                && getLongitude().equals(that.getLongitude()) && getLatitude().equals(that.getLatitude()) && speed.equals(that.speed)
                && course.equals(that.course) && getPositionLatency().equals(that.getPositionLatency()) && getSpare().equals(that.getSpare());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPositionAccuracy(), getRaim(), navigationStatus, getLongitude(), getLatitude(), speed, course,
                getPositionLatency(), getSpare());
    }

    public AISMessageType getMessageType() {
        return AISMessageType.LONG_RANGE_BROADCAST_MESSAGE;
    }

    @Override
    public TransponderClass getTransponderClass() {
        return TransponderClass.A;
    }

    /**
     * @return true if position accuracy lte 10m; false otherwise.
     */
    @SuppressWarnings("unused")
	public Boolean getPositionAccuracy() {
        return getDecodedValue(() -> positionAccuracy, value -> positionAccuracy = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(38, 39)));
	}

    public Boolean getRaim() {
        return getDecodedValue(() -> raim, value -> raim = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(39, 40)));
	}

	public NavigationStatus getNavigationalStatus() {
        return getDecodedValue(() -> navigationStatus, value -> navigationStatus = value, () -> Boolean.TRUE,
                () -> NavigationStatus.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 44))));
	}

	public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(44, 62)) / 600f);
	}

	public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(62, 79)) / 600f);
	}

    /**
     * @return Knots (0-62); 63 = not available = default
     */
    @SuppressWarnings("unused")
	public Float getSpeedOverGround() {
        return Float.valueOf(getDecodedValue(() -> speed, value -> speed = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(79, 85))));
	}


    /**
     * @return Degrees (0-359); 511 = not available = default
     */
    @SuppressWarnings("unused")
	public Float getCourseOverGround() {
        return Float.valueOf(getDecodedValue(() -> course, value -> course = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(85, 94))));
	}

    /**
     * @return 0 if reported position latency is less than 5 seconds; 1 if reported position latency is greater than 5 seconds = default
     */
	public Integer getPositionLatency() {
        return getDecodedValue(() -> positionLatency, value -> positionLatency = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(94, 95)));
	}

	public Integer getSpare() {
        return getDecodedValue(() -> spare, value -> spare = value, () -> Boolean.TRUE,
                () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(95, 96)));
	}

    @Override
    public String toString() {
        return "LongRangeBroadcastMessage{" +
                "messageType=" + getMessageType() +
                ", positionAccuracy=" + getPositionAccuracy() +
                ", raim=" + getRaim() +
                ", status=" + getNavigationalStatus() +
                ", longitude=" + getLongitude() +
                ", latitude=" + getLatitude() +
                ", speed=" + getSpeedOverGround() +
                ", course=" + getCourseOverGround() +
                ", positionLatency=" + getPositionLatency() +
                ", spare=" + getSpare() +
                "} " + super.toString();
    }

}