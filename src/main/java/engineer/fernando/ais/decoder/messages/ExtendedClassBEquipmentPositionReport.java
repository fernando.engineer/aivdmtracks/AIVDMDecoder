/*
 * ExtendedClassBEquipmentPositionReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.PositionFixingDevice;
import engineer.fernando.ais.decoder.messages.types.ShipType;
import engineer.fernando.ais.decoder.messages.types.TransponderClass;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

@SuppressWarnings("serial")
public class ExtendedClassBEquipmentPositionReport extends AISMessage implements ExtendedDynamicDataReport {

    private transient String regionalReserved1;
    private transient Float speedOverGround;
    private transient Boolean positionAccurate;
    private transient Float latitude;
    private transient Float longitude;
    private transient Float courseOverGround;
    private transient Integer trueHeading;
    private transient Integer second;
    private transient String regionalReserved2;
    private transient String shipName;
    private transient ShipType shipType;
    private transient Integer toBow;
    private transient Integer toStern;
    private transient Integer toStarboard;
    private transient Integer toPort;
    private transient PositionFixingDevice positionFixingDevice;
    private transient Boolean raimFlag;
    private transient Boolean dataTerminalReady;
    private transient Boolean assigned;

    public ExtendedClassBEquipmentPositionReport(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public ExtendedClassBEquipmentPositionReport(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExtendedClassBEquipmentPositionReport)) return false;
        if (!super.equals(o)) return false;
        ExtendedClassBEquipmentPositionReport that = (ExtendedClassBEquipmentPositionReport) o;
        return getRegionalReserved1().equals(that.getRegionalReserved1()) && getSpeedOverGround().equals(that.getSpeedOverGround())
                && getPositionAccurate().equals(that.getPositionAccurate()) && getLatitude().equals(that.getLatitude())
                && getLongitude().equals(that.getLongitude()) && getCourseOverGround().equals(that.getCourseOverGround())
                && getTrueHeading().equals(that.getTrueHeading()) && getSecond().equals(that.getSecond())
                && getRegionalReserved2().equals(that.getRegionalReserved2()) && getShipName().equals(that.getShipName())
                && getShipType() == that.getShipType() && getToBow().equals(that.getToBow()) && getToStern().equals(that.getToStern())
                && getToStarboard().equals(that.getToStarboard()) && getToPort().equals(that.getToPort())
                && getPositionFixingDevice() == that.getPositionFixingDevice() && getRaimFlag().equals(that.getRaimFlag())
                && getDataTerminalReady().equals(that.getDataTerminalReady()) && getAssigned().equals(that.getAssigned());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getRegionalReserved1(), getSpeedOverGround(), getPositionAccurate(), getLatitude(),
                getLongitude(), getCourseOverGround(), getTrueHeading(), getSecond(), getRegionalReserved2(), getShipName(), getShipType(),
                getToBow(), getToStern(), getToStarboard(), getToPort(), getPositionFixingDevice(), getRaimFlag(), getDataTerminalReady(), getAssigned());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.EXTENDED_CLASS_B_EQUIPMENT_POSITION_REPORT;
    }

    @Override
    public TransponderClass getTransponderClass() {
        return TransponderClass.B;
    }

	public String getRegionalReserved1() {
        return getDecodedValue(() -> regionalReserved1, value -> regionalReserved1 = value, () -> Boolean.TRUE, () -> BIT_DECODER.apply(getBits(38, 46)));
	}

	public Float getSpeedOverGround() {
        return getDecodedValue(() -> speedOverGround, value -> speedOverGround = value, () -> Boolean.TRUE, () -> UNSIGNED_FLOAT_DECODER.apply(getBits(46, 56)) / 10f);
	}

	public Boolean getPositionAccurate() {
        return getDecodedValue(() -> positionAccurate, value -> positionAccurate = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(56, 57)));
	}

	public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(85, 112)) / 600000f);
	}

	public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(57, 85)) / 600000f);
	}

	public Float getCourseOverGround() {
        return getDecodedValue(() -> courseOverGround, value -> courseOverGround = value, () -> Boolean.TRUE, () -> UNSIGNED_FLOAT_DECODER.apply(getBits(112, 124)) / 10f);
	}

	public Integer getTrueHeading() {
        return getDecodedValue(() -> trueHeading, value -> trueHeading = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(124, 133)));
	}

	public Integer getSecond() {
        return getDecodedValue(() -> second, value -> second = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(133, 139)));
	}

	public String getRegionalReserved2() {
        return getDecodedValue(() -> regionalReserved2, value -> regionalReserved2 = value, () -> Boolean.TRUE, () -> BIT_DECODER.apply(getBits(139, 143)));
	}

	public String getShipName() {
        return getDecodedValue(() -> shipName, value -> shipName = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(143, 263)));
	}

	public ShipType getShipType() {
        return getDecodedValue(() -> shipType, value -> shipType = value, () -> Boolean.TRUE,
                () -> ShipType.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(263, 271))));
	}

	public Integer getToBow() {
        return getDecodedValue(() -> toBow, value -> toBow = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(271, 280)));
	}

	public Integer getToStern() {
        return getDecodedValue(() -> toStern, value -> toStern = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(280, 289)));
	}

	public Integer getToStarboard() {
        return getDecodedValue(() -> toStarboard, value -> toStarboard = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(295, 301)));
	}

	public Integer getToPort() {
        return getDecodedValue(() -> toPort, value -> toPort = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(289, 295)));
	}

	public PositionFixingDevice getPositionFixingDevice() {
        return getDecodedValue(() -> positionFixingDevice, value -> positionFixingDevice = value,
                () -> Boolean.TRUE, () -> PositionFixingDevice.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(301, 305))));
	}

	public Boolean getRaimFlag() {
        return getDecodedValue(() -> raimFlag, value -> raimFlag = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(305, 306)));
	}

	public Boolean getDataTerminalReady() {
        return getDecodedValue(() -> dataTerminalReady, value -> dataTerminalReady = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(306, 307)));
	}

	public Boolean getAssigned() {
        return getDecodedValue(() -> assigned, value -> assigned = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(307, 308)));
	}
    public String getRegionalReserved3() {
        return getDecodedValue(() -> regionalReserved1, value -> regionalReserved1 = value, () -> Boolean.TRUE, () -> BIT_DECODER.apply(getBits(308, 312)));
    }

    @Override
    public String toString() {
        return "ExtendedClassBEquipmentPositionReport{" +
                "messageType=" + getMessageType() +
                ", regionalReserved1='" + getRegionalReserved1() + '\'' +
                ", speedOverGround=" + getSpeedOverGround() +
                ", positionAccurate=" + getPositionAccurate() +
                ", latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", courseOverGround=" + getCourseOverGround() +
                ", trueHeading=" + getTrueHeading() +
                ", second=" + getSecond() +
                ", regionalReserved2='" + getRegionalReserved2() + '\'' +
                ", shipName='" + getShipName() + '\'' +
                ", shipType=" + getShipType() +
                ", toBow=" + getToBow() +
                ", toStern=" + getToStern() +
                ", toStarboard=" + getToStarboard() +
                ", toPort=" + getToPort() +
                ", positionFixingDevice=" + getPositionFixingDevice() +
                ", raimFlag=" + getRaimFlag() +
                ", dataTerminalReady=" + getDataTerminalReady() +
                ", assigned=" + getAssigned() +
                "} " + super.toString();
    }

}