/*
 * PositionFixingDevice
 */

package engineer.fernando.ais.decoder.messages.types;

public enum PositionFixingDevice {
	POSITION_FIXING_DEVICE(0),
	GPS(1),
	GLONASS(2),
	COMBINED_GPS_GLONASS(3),
	LORAN_C(4),
	CHAYKA(5),
	INTEGRATED_NAVIGATION_SYSTEM(6),
	SURVEYED(7),
	GALILEO(8);

	PositionFixingDevice(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static PositionFixingDevice fromInteger(Integer integer) {
		if (integer != null) {
			for (PositionFixingDevice b : PositionFixingDevice.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}