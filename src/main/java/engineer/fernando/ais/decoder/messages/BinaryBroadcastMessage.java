/*
 * BinaryBroadcastMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.specific.ApplicationSpecificMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

import java.lang.ref.WeakReference;
import java.util.Objects;

/**
 * broadcast message with unspecified binary payload. The St. Lawrence Seaway
 * AIS system and the USG PAWSS system use this payload for local extension
 * messages. It is variable in length up to a maximum of 1008 bits (up to 5
 * AIVDM sentence payloads).
 *
 */
@SuppressWarnings("serial")
public class BinaryBroadcastMessage extends AISMessage {

    private transient Integer spare;
    private transient Integer designatedAreaCode;
    private transient Integer functionalId;
    private transient WeakReference<String> binaryData;
    private transient WeakReference<ApplicationSpecificMessage> applicationSpecificMessage;

    public BinaryBroadcastMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public BinaryBroadcastMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinaryBroadcastMessage)) return false;
        if (!super.equals(o)) return false;
        BinaryBroadcastMessage that = (BinaryBroadcastMessage) o;
        return getSpare().equals(that.getSpare()) && getDesignatedAreaCode().equals(that.getDesignatedAreaCode())
                && getFunctionalId().equals(that.getFunctionalId()) && getBinaryData().equals(that.getBinaryData())
                && getApplicationSpecificMessage().equals(that.getApplicationSpecificMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSpare(), getDesignatedAreaCode(), getFunctionalId(),
                getBinaryData(), getApplicationSpecificMessage());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.BINARY_BROADCAST_MESSAGE;
    }

	public Integer getSpare() {
        return getDecodedValue(() -> spare, ref -> spare = ref, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

	public Integer getDesignatedAreaCode() {
        return getDecodedValue(() -> designatedAreaCode, value -> designatedAreaCode = value,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 50)));
	}

	public Integer getFunctionalId() {
        return getDecodedValue(() -> functionalId, value -> functionalId = value,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(50, 56)));
	}

	public String getBinaryData() {
        return getDecodedValueByWeakReference(() -> binaryData, value -> binaryData = value, () -> Boolean.TRUE, () -> Decoders.BIT_DECODER.apply(getBits(56, getNumberOfBits())));
	}

    public ApplicationSpecificMessage getApplicationSpecificMessage() {
        ApplicationSpecificMessage asm = this.applicationSpecificMessage == null ? null : this.applicationSpecificMessage.get();
        if (asm == null) {
            asm = ApplicationSpecificMessage.create(getDesignatedAreaCode(), getFunctionalId(), getBinaryData());
            applicationSpecificMessage = new WeakReference<>(asm);
        }

        if (asm.getDesignatedAreaCode() != this.getDesignatedAreaCode().intValue())
            throw new IllegalStateException("Implementation error: DAC of AISMessage does not match ASM: " + asm.getDesignatedAreaCode() + " " + this.getDesignatedAreaCode());

        if (asm.getFunctionalId() != this.getFunctionalId().intValue())
            throw new IllegalStateException("Implementation error: FI of AISMessage does not match ASM: " + asm.getFunctionalId() + " " + this.getFunctionalId());

        return asm;
    }

    @Override
    public String toString() {
        return "BinaryBroadcastMessage{" +
                "messageType=" + getMessageType() +
                ", spare=" + getSpare() +
                ", designatedAreaCode=" + getDesignatedAreaCode() +
                ", functionalId=" + getFunctionalId() +
                ", binaryData='" + getBinaryData() + '\'' +
                "} " + super.toString();
    }

}