/*
 * MessageType
 */

package engineer.fernando.ais.decoder.messages.types;

import engineer.fernando.ais.decoder.messages.*;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public enum AISMessageType {

	POSITION_REPORT_CLASS_A_SCHEDULED(1),
	POSITION_REPORT_CLASS_A_ASSIGNED_SCHEDULE(2),
	POSITION_REPORT_CLASS_A_RESPONSE_TO_INTERROGATION(3),
	BASE_STATION_REPORT(4),
	SHIP_AND_VOYAGE_RELATED_DATA(5),
	ADDRESSED_BINARY_MESSAGE(6),
	BINARY_ACKNOWLEDGE(7),
	BINARY_BROADCAST_MESSAGE(8),
	STANDARD_SAR_AIRCRAFT_POSITION_REPORT(9),
	UTC_AND_DATE_INQUIRY(10),
	UTC_AND_DATE_RESPONSE(11),
	ADDRESSED_SAFETY_RELATED_MESSAGE(12),
	SAFETY_RELATED_ACKNOWLEDGE(13),
	SAFETY_RELATED_BROADCAST_MESSAGE(14),
	INTERROGATION(15),
	ASSIGNED_MODE_COMMAND(16),
	GNSS_BINARY_BROADCAST_MESSAGE(17),
	STANDARD_CLASS_BCS_POSITION_REPORT(18),
	EXTENDED_CLASS_B_EQUIPMENT_POSITION_REPORT(19),
	DATA_LINK_MANAGEMENT(20),
	AID_TO_NAVIGATION_REPORT(21),
	CHANNEL_MANAGEMENT(22),
	GROUP_ASSIGNMENT_COMMAND(23),
	CLASS_BCS_STATIC_DATA_REPORT(24),
	BINARY_MESSAGE_SINGLE_SLOT(25),
	BINARY_MESSAGE_MULTIPLE_SLOT(26),
	LONG_RANGE_BROADCAST_MESSAGE(27),
	ERROR(-1);

	public static final int MIN_CODE = 1;
	public static final int MAX_CODE = 27;

	// Map of Classes according its code.
	public static final Map<Integer, Class<?>> AIS_MESSAGES_CLASSES = Map.ofEntries(
			new AbstractMap.SimpleImmutableEntry<>(1, PositionReportClassAScheduled.class),
			new AbstractMap.SimpleImmutableEntry<>(2, PositionReportClassAAssignedSchedule.class),
			new AbstractMap.SimpleImmutableEntry<>(3, PositionReportClassAResponseToInterrogation.class),
			new AbstractMap.SimpleImmutableEntry<>(4, BaseStationReport.class),
			new AbstractMap.SimpleImmutableEntry<>(5, ShipAndVoyageData.class),
			new AbstractMap.SimpleImmutableEntry<>(6, AddressedBinaryMessage.class),
			new AbstractMap.SimpleImmutableEntry<>(7, BinaryAcknowledge.class),
			new AbstractMap.SimpleImmutableEntry<>(8, BinaryBroadcastMessage.class),
			new AbstractMap.SimpleImmutableEntry<>(9, StandardSARAircraftPositionReport.class),
			new AbstractMap.SimpleImmutableEntry<>(10, UTCAndDateInquiry.class),
			new AbstractMap.SimpleImmutableEntry<>(11, UTCAndDateResponse.class),
			new AbstractMap.SimpleImmutableEntry<>(12, AddressedSafetyRelatedMessage.class),
			new AbstractMap.SimpleImmutableEntry<>(13, SafetyRelatedAcknowledge.class),
			new AbstractMap.SimpleImmutableEntry<>(14, SafetyRelatedBroadcastMessage.class),
			new AbstractMap.SimpleImmutableEntry<>(15, Interrogation.class),
			new AbstractMap.SimpleImmutableEntry<>(16, AssignedModeCommand.class),
			new AbstractMap.SimpleImmutableEntry<>(17, GNSSBinaryBroadcastMessage.class),
			new AbstractMap.SimpleImmutableEntry<>(18, StandardClassBCSPositionReport.class),
			new AbstractMap.SimpleImmutableEntry<>(19, ExtendedClassBEquipmentPositionReport.class),
			new AbstractMap.SimpleImmutableEntry<>(20, DataLinkManagement.class),
			new AbstractMap.SimpleImmutableEntry<>(21, AidToNavigationReport.class),
			new AbstractMap.SimpleImmutableEntry<>(22, ChannelManagement.class),
			new AbstractMap.SimpleImmutableEntry<>(23, GroupAssignmentCommand.class),
			new AbstractMap.SimpleImmutableEntry<>(24, ClassBCSStaticDataReport.class),
			new AbstractMap.SimpleImmutableEntry<>(25, BinaryMessageSingleSlot.class),
			new AbstractMap.SimpleImmutableEntry<>(26, BinaryMessageMultipleSlot.class),
			new AbstractMap.SimpleImmutableEntry<>(27, LongRangeBroadcastMessage.class) );

	public static final Map<Integer, List<Integer>> LENGTHS = Map.ofEntries(
			new AbstractMap.SimpleImmutableEntry<>(1, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(2, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(3, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(4, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList(422,424)),
			new AbstractMap.SimpleImmutableEntry<>(6, Collections.singletonList(1008)),
			new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList(72,104,136,168)),
			new AbstractMap.SimpleImmutableEntry<>(8, Collections.singletonList(1008)),
			new AbstractMap.SimpleImmutableEntry<>(9, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(10, Collections.singletonList(72)),
			new AbstractMap.SimpleImmutableEntry<>(11, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(12, Collections.singletonList(1008)),
			new AbstractMap.SimpleImmutableEntry<>(13, Arrays.asList(72,104,136,168)),
			new AbstractMap.SimpleImmutableEntry<>(14, Collections.singletonList(1008)),
			new AbstractMap.SimpleImmutableEntry<>(15, Arrays.asList(88,110,112,160)),
			new AbstractMap.SimpleImmutableEntry<>(16, Arrays.asList(96,144)),
			new AbstractMap.SimpleImmutableEntry<>(17, Arrays.asList(80,816)),
			new AbstractMap.SimpleImmutableEntry<>(18, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(19, Collections.singletonList(312)),
			new AbstractMap.SimpleImmutableEntry<>(20, Arrays.asList(72,160)),
			new AbstractMap.SimpleImmutableEntry<>(21, Arrays.asList(272,360)),
			new AbstractMap.SimpleImmutableEntry<>(22, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(23, Collections.singletonList(160)),
			new AbstractMap.SimpleImmutableEntry<>(24, Arrays.asList(158,160,168)),
			new AbstractMap.SimpleImmutableEntry<>(25, Collections.singletonList(168)),
			new AbstractMap.SimpleImmutableEntry<>(27, Arrays.asList(96,168)) );

	public static String checkNumberOfBits(AISMessageType messageType, int numberOfBits)  {
		if(messageType.getCode() < 1 || messageType.getCode() > 27 ){
			return "Unsuported Message";
		}
		if(IntStream.of(numberOfBits).noneMatch(PREDICATES.get(messageType.getCode()))){
			return messageSizeRule(messageType,BYTESIZE_MESSAGE_ARG.get(messageType.getCode()), numberOfBits);
		}
		return "";
	}

	private static IntPredicate oneOfList(List<Integer> options){
		return bits -> options.stream().mapToInt(Integer::intValue).anyMatch(option -> option == bits);
	}

	private static IntPredicate betweenInclusive(int min, int max){
		return bits -> bits >= min && bits <= max;
	}

	private static IntPredicate lowerThanInclusive(int max){
		return bits -> bits <= max;
	}

	public static final Map<Integer, IntPredicate> PREDICATES = Map.ofEntries(
			new AbstractMap.SimpleImmutableEntry<>(1, oneOfList(LENGTHS.get(1))),
			new AbstractMap.SimpleImmutableEntry<>(2, oneOfList(LENGTHS.get(2))),
			new AbstractMap.SimpleImmutableEntry<>(3, oneOfList(LENGTHS.get(3))),
			new AbstractMap.SimpleImmutableEntry<>(4, oneOfList(LENGTHS.get(4))),
			new AbstractMap.SimpleImmutableEntry<>(5, oneOfList(LENGTHS.get(5))),
			new AbstractMap.SimpleImmutableEntry<>(6, betweenInclusive(72,1008)),
			new AbstractMap.SimpleImmutableEntry<>(7, oneOfList(LENGTHS.get(7))),
			new AbstractMap.SimpleImmutableEntry<>(8, betweenInclusive(56,1008)),
			new AbstractMap.SimpleImmutableEntry<>(9, oneOfList(LENGTHS.get(9))),
			new AbstractMap.SimpleImmutableEntry<>(10, oneOfList(LENGTHS.get(10))),
			new AbstractMap.SimpleImmutableEntry<>(11, oneOfList(LENGTHS.get(11))),
			new AbstractMap.SimpleImmutableEntry<>(12, lowerThanInclusive(1008)),
			new AbstractMap.SimpleImmutableEntry<>(13, oneOfList(LENGTHS.get(13))),
			new AbstractMap.SimpleImmutableEntry<>(14, lowerThanInclusive(1008)),
			new AbstractMap.SimpleImmutableEntry<>(15, oneOfList(LENGTHS.get(15))),
			new AbstractMap.SimpleImmutableEntry<>(16, oneOfList(LENGTHS.get(16))),
			new AbstractMap.SimpleImmutableEntry<>(17, betweenInclusive(80,816)),
			new AbstractMap.SimpleImmutableEntry<>(18, oneOfList(LENGTHS.get(18))),
			new AbstractMap.SimpleImmutableEntry<>(19, oneOfList(LENGTHS.get(19))),
			new AbstractMap.SimpleImmutableEntry<>(20, betweenInclusive(72,160)),
			new AbstractMap.SimpleImmutableEntry<>(21, betweenInclusive(272,360)),
			new AbstractMap.SimpleImmutableEntry<>(22, oneOfList(LENGTHS.get(22))),
			new AbstractMap.SimpleImmutableEntry<>(23, oneOfList(LENGTHS.get(23))),
			new AbstractMap.SimpleImmutableEntry<>(24, oneOfList(LENGTHS.get(24))),
			new AbstractMap.SimpleImmutableEntry<>(25, lowerThanInclusive(168)),
			new AbstractMap.SimpleImmutableEntry<>(26, lowerThanInclusive(1064)),
			new AbstractMap.SimpleImmutableEntry<>(27, oneOfList(LENGTHS.get(27)))
			 );

	public static String messageSizeRule(AISMessageType type, String rule, int actual) {
		return String.format("Message of type %s should be %s bits long; not %d.", type, rule, actual);
	}

	private static final Map<Integer, String> BYTESIZE_MESSAGE_ARG = Map.ofEntries(
			new AbstractMap.SimpleImmutableEntry<>(1, exactlyArgument(LENGTHS.get(1))),
			new AbstractMap.SimpleImmutableEntry<>(2, exactlyArgument(LENGTHS.get(2))),
			new AbstractMap.SimpleImmutableEntry<>(3, exactlyArgument(LENGTHS.get(3))),
			new AbstractMap.SimpleImmutableEntry<>(4, exactlyArgument(LENGTHS.get(4))),
			new AbstractMap.SimpleImmutableEntry<>(5, exactlyArgument(LENGTHS.get(5))),
			new AbstractMap.SimpleImmutableEntry<>(6, betweenArgument(72,1008)),
			new AbstractMap.SimpleImmutableEntry<>(7, exactlyArgument(LENGTHS.get(7))),
			new AbstractMap.SimpleImmutableEntry<>(8, betweenArgument(56,1008)),
			new AbstractMap.SimpleImmutableEntry<>(9, exactlyArgument(LENGTHS.get(9))),
			new AbstractMap.SimpleImmutableEntry<>(10, exactlyArgument(LENGTHS.get(10))),
			new AbstractMap.SimpleImmutableEntry<>(11, exactlyArgument(LENGTHS.get(11))),
			new AbstractMap.SimpleImmutableEntry<>(12, atMostArgument(168)),
			new AbstractMap.SimpleImmutableEntry<>(13, exactlyArgument(LENGTHS.get(13))),
			new AbstractMap.SimpleImmutableEntry<>(14, atMostArgument(1008)),
			new AbstractMap.SimpleImmutableEntry<>(15, exactlyArgument(LENGTHS.get(15))),
			new AbstractMap.SimpleImmutableEntry<>(16, exactlyArgument(LENGTHS.get(16))),
			new AbstractMap.SimpleImmutableEntry<>(17, betweenArgument(80,816)),
			new AbstractMap.SimpleImmutableEntry<>(18, exactlyArgument(LENGTHS.get(18))),
			new AbstractMap.SimpleImmutableEntry<>(19, exactlyArgument(LENGTHS.get(19))),
			new AbstractMap.SimpleImmutableEntry<>(20, betweenArgument(72,160)),
			new AbstractMap.SimpleImmutableEntry<>(21, betweenArgument(272,360)),
			new AbstractMap.SimpleImmutableEntry<>(22, exactlyArgument(LENGTHS.get(22))),
			new AbstractMap.SimpleImmutableEntry<>(23, exactlyArgument(LENGTHS.get(23))),
			new AbstractMap.SimpleImmutableEntry<>(24, exactlyArgument(LENGTHS.get(24))),
			new AbstractMap.SimpleImmutableEntry<>(25, atMostArgument(168)),
			new AbstractMap.SimpleImmutableEntry<>(26, atMostArgument(1064)),
			new AbstractMap.SimpleImmutableEntry<>(27, exactlyArgument(LENGTHS.get(27)))
	);

	private static String exactlyArgument(List<Integer> alternatives){
		StringBuilder sb = new StringBuilder();
		for (int i=0; i< alternatives.size(); i++) {
			sb.append(i);
			if (i != alternatives.size()-1) sb.append(" or ");
		}
		return sb.toString();
	}

	private static String atMostArgument(int max){
		return new StringBuilder().append("at most ").append(max).toString();
	}

	private static String betweenArgument(int min, int max){
		return new StringBuilder().append("at least ").append(min).append(" and at most ").append(max).toString();
	}

	AISMessageType(Integer code) {
		this.code = code;
	}

	private final Integer code;

	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	public static AISMessageType fromInteger(Integer integer) {
		if (integer != null) {
			for (AISMessageType b : AISMessageType.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}

}