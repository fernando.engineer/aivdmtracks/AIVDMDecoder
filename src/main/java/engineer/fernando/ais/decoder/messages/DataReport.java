/*
 * DataReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.TransponderClass;

/**
 * AIS messages with a payload of ship static or voyage related data
 * or position reports implement this interface.
 */
public interface DataReport {
	TransponderClass getTransponderClass();
}