/*
 * AidToNavigationReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.AidType;
import engineer.fernando.ais.decoder.messages.types.PositionFixingDevice;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;
import java.util.Objects;

/**
 * Identification and location message to be emitted by aids to navigation.
 */

public class AidToNavigationReport extends AISMessage {

    private transient AidType aidType;
    private transient String name;
    private transient Boolean positionAccurate;
    private transient Float latitude;
    private transient Float longitude;
    private transient Integer toBow;
    private transient Integer toStern;
    private transient Integer toPort;
    private transient Integer toStarboard;
    private transient PositionFixingDevice positionFixingDevice;
    private transient Integer second;
    private transient Boolean offPosition;
    private transient String regionalUse;
    private transient Boolean raimFlag;
    private transient Boolean virtualAid;
    private transient Boolean assignedMode;
    private transient Integer spare1;
    private transient String nameExtension;
    private transient Integer spare2;

    public AidToNavigationReport(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public AidToNavigationReport(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AidToNavigationReport)) return false;
        if (!super.equals(o)) return false;
        AidToNavigationReport that = (AidToNavigationReport) o;
        return getAidType() == that.getAidType() && getName().equals(that.getName())
                && getPositionAccurate().equals(that.getPositionAccurate()) && getLatitude().equals(that.getLatitude())
                && getLongitude().equals(that.getLongitude()) && getToBow().equals(that.getToBow())
                && getToStern().equals(that.getToStern()) && getToPort().equals(that.getToPort())
                && getToStarboard().equals(that.getToStarboard()) && getPositionFixingDevice() == that.getPositionFixingDevice()
                && getSecond().equals(that.getSecond()) && getOffPosition().equals(that.getOffPosition())
                && regionalUse.equals(that.regionalUse) && getRaimFlag().equals(that.getRaimFlag())
                && getVirtualAid().equals(that.getVirtualAid()) && getAssignedMode().equals(that.getAssignedMode())
                && getSpare1().equals(that.getSpare1()) && getNameExtension().equals(that.getNameExtension()) && getSpare2().equals(that.getSpare2());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAidType(), getName(), getPositionAccurate(), getLatitude(), getLongitude(), getToBow(),
                getToStern(), getToPort(), getToStarboard(), getPositionFixingDevice(), getSecond(), getOffPosition(), regionalUse,
                getRaimFlag(), getVirtualAid(), getAssignedMode(), getSpare1(), getNameExtension(), getSpare2());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.AID_TO_NAVIGATION_REPORT;
    }

    public AidType getAidType() {
        return getDecodedValue(() -> aidType, value -> aidType = value, () -> Boolean.TRUE, () -> AidType.fromInteger(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 43))));
    }

    public String getName() {
        return getDecodedValue(() -> name, value -> name = value, () -> Boolean.TRUE, () -> Decoders.STRING_DECODER.apply(getBits(43, 163)));
    }

    public Boolean getPositionAccurate() {
        return getDecodedValue(() -> positionAccurate, value -> positionAccurate = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(163, 164)));
    }

    public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> Decoders.FLOAT_DECODER.apply(getBits(192, 219))/600000f);
    }

    public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> Decoders.FLOAT_DECODER.apply(getBits(164, 192))/600000f);
    }

    public Integer getToBow() {
        return getDecodedValue(() -> toBow, value -> toBow = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(219, 228)));
    }

    public Integer getToStern() {
        return getDecodedValue(() -> toStern, value -> toStern = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(228, 237)));
    }

    public Integer getToStarboard() {
        return getDecodedValue(() -> toStarboard, value -> toStarboard = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(243, 249)));
    }

    public Integer getToPort() {
        return getDecodedValue(() -> toPort, value -> toPort = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(237, 243)));
    }

    public PositionFixingDevice getPositionFixingDevice() {
        return getDecodedValue(() -> positionFixingDevice, value -> positionFixingDevice = value, () -> Boolean.TRUE,
                () -> PositionFixingDevice.fromInteger(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(249, 253))));
    }

    public Integer getSecond() {
        return getDecodedValue(() -> second, value -> second = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(253, 259)));
    }

    public Boolean getOffPosition() {
        return getDecodedValue(() -> offPosition, value -> offPosition = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(259, 260)));
    }

    public String getAtoNStatus() {
        return getDecodedValue(() -> regionalUse, value -> regionalUse = value, () -> Boolean.TRUE, () -> Decoders.BIT_DECODER.apply(getBits(260, 268)));
    }

    public Boolean getRaimFlag() {
        return getDecodedValue(() -> raimFlag, value -> raimFlag = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(268, 269)));
    }

    public Boolean getVirtualAid() {
        return getDecodedValue(() -> virtualAid, value -> virtualAid = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(269, 270)));
    }

    public Boolean getAssignedMode() {
        return getDecodedValue(() -> assignedMode, value -> assignedMode = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(270, 271)));
    }

    public Integer getSpare1() {
        return getDecodedValue(() -> spare1, value -> spare1 = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(271, 272)));
    }

    public String getNameExtension() {
        getDecodedValue(() -> nameExtension, value -> nameExtension = value, () -> getNumberOfBits() > 272, () -> {
            int extraBits = getNumberOfBits() - 272;
            int extraChars = extraBits/6;
            int extraBitsOfChars = extraChars*6;
            return Decoders.STRING_DECODER.apply(getBits(272, 272 + extraBitsOfChars));
        });
        return nameExtension;
    }

    public Integer getSpare2() {
        getDecodedValue(() -> spare2, value -> spare2 = value, () -> getNumberOfBits() >= 272, () -> {
            int extraBits = getNumberOfBits() - 272;
            int extraChars = extraBits/6;
            int extraBitsOfChars = extraChars*6;
            return (extraBits == extraBitsOfChars) ? 0 : Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(272 + extraBitsOfChars, getNumberOfBits()));
        });
        return spare2;
    }

    @Override
    public String toString() {
        return "AidToNavigationReport{" +
                "messageType=" + getMessageType() +
                ", aidType=" + getAidType() +
                ", name='" + getName() + '\'' +
                ", positionAccurate=" + getPositionAccurate() +
                ", latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", toBow=" + getToBow() +
                ", toStern=" + getToStern() +
                ", toPort=" + getToPort() +
                ", toStarboard=" + getToStarboard() +
                ", positionFixingDevice=" + getPositionFixingDevice() +
                ", second=" + getSecond() +
                ", offPosition=" + getOffPosition() +
                ", regionalUse='" + getAtoNStatus() + '\'' +
                ", raimFlag=" + getRaimFlag() +
                ", virtualAid=" + getVirtualAid() +
                ", assignedMode=" + getAssignedMode() +
                ", spare1=" + getSpare1() +
                ", nameExtension='" + getNameExtension() + '\'' +
                ", spare2=" + getSpare2() +
                "} " + super.toString();
    }

}