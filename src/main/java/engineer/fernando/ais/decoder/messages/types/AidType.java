/*
 * AidType
 */

package engineer.fernando.ais.decoder.messages.types;

public enum AidType {
	UNSPECIFIED(0),
	REFERENCE_POINT(1),
	RACON(2),
	FIXED_STRUCTURE(3),
	FUTURE_USE_1(4),
	LIGHT_WITHOUT_SECTORS(5),
	LIGHT_WITH_SECTORS(6),
	LEADING_LIGHT_FRONT(7),
	LEADING_LIGHT_REAR(8),
	BEACON_CARDINAL_NORTH(9),
	BEACON_CARDINAL_EAST(10),
	BEACON_CARDINAL_SOUTH(11),
	BEACON_CARDINAL_WEST(12),
	BEACON_PORT_HAND(13),
	BEACON_STARBOARD_HAND(14),
	BEACON_PREFERRED_CHANNEL_PORT_HAND(15),
	BEACON_PREFERRED_CHANNEL_STARBOARD_HAND(16),
	BEACON_ISOLATED_DANGER(17),
	BEACON_SAFE_WATER(18),
	BEACON_SPECIAL_MARK(19),
	CARDINAL_MARK_NORTH(20),
	CARDINAL_MARK_EAST(21),
	CARDINAL_MARK_SOUTH(22),
	CARDINAL_MARK_WEST(23),
	PORT_HAND_MARK(24),
	STARBOARD_HAND_MARK(25),
	PREFERRED_CHANNEL_PORT_HAND(26),
	PREFERRED_CHANNEL_STARBOARD_HAND(27),
	ISOLATED_DANGER(28),
	SAFE_WATER(29),
	SPECIAL_MARK(30),
	LIGHT_VESSEL(31);

	AidType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static AidType fromInteger(Integer integer) {
		if (integer != null) {
			for (AidType b : AidType.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}