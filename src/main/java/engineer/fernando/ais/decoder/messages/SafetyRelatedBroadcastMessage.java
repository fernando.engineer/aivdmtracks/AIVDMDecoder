/*
 * SafetyRelatedBroadcastMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import static engineer.fernando.ais.decoder.util.Decoders.STRING_DECODER;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;

@SuppressWarnings("serial")
public class SafetyRelatedBroadcastMessage extends AISMessage {

    private transient Integer spare;
    private transient String text;

    public SafetyRelatedBroadcastMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public SafetyRelatedBroadcastMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.SAFETY_RELATED_BROADCAST_MESSAGE;
    }

    @SuppressWarnings("unused")
	public Integer getSpare() {
        return getDecodedValue(() -> spare, value -> spare = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

    @SuppressWarnings("unused")
	public final String getText() {
        return getDecodedValue(() -> text, value -> text = value, () -> Boolean.TRUE, () -> {
            int extraBitsOfChars = ((getNumberOfBits() - 40) / 6) * 6;
            return STRING_DECODER.apply(getBits(40, 40 + extraBitsOfChars));
        });
	}

    @Override
    public String toString() {
        return "SafetyRelatedBroadcastMessage{" +
                "messageType=" + getMessageType() +
                ", spare=" + getSpare() +
                ", text='" + getText() + '\'' +
                "} " + super.toString();
    }

}