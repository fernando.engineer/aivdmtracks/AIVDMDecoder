package engineer.fernando.ais.decoder.messages.types;

import engineer.fernando.ais.decoder.exceptions.InvalidTagBlock;
import engineer.fernando.ais.decoder.exceptions.NMEAParseException;
import engineer.fernando.ais.decoder.util.StringTools;

import java.util.*;

public class NMEATagBlock {
    private final Long timestamp;
    private final String destinationId;
    private final String sentenceGrouping;
    private final Integer lineCount;
    private final Long relativeTime;
    private final String sourceId;
    private final String text;
    private final Integer checksum;
    private final String rawMessage;
    private final Boolean valid;
    private Map<TAGBlockParameterCodeType, NMEATagBlockParameterCode> parameterMap;

    public final Long getTimestamp() {
        return timestamp;
    }

    public final String getDestinationId() {
        return destinationId;
    }

    public final String getSentenceGrouping() {
        return sentenceGrouping;
    }

    public final Integer getLineCount() {
        return lineCount;
    }

    public final Long getRelativeTime() {
        return relativeTime;
    }

    public final String getSourceId() {
        return sourceId;
    }

    public final String getText() {
        return text;
    }

    public final String getRawMessage() {
        return rawMessage;
    }

    public static NMEATagBlock fromString(String nmeaTagBlockString) {
        return new NMEATagBlock(nmeaTagBlockString);
    }

    private NMEATagBlock(String rawMessage) {
        final var nmeaTagBlockRegEx = "^\\\\.*\\*[0-9A-Fa-f]{2}\\\\$";

        if (! rawMessage.matches(nmeaTagBlockRegEx))
            throw new NMEAParseException(rawMessage, "Message does not comply with regexp \"" + nmeaTagBlockRegEx + "\"");

        var rawMessageCleaned = rawMessage.substring(1, rawMessage.length() - 1);
        String[] msg = rawMessageCleaned.split("\\*");

        if (msg.length != 2)
            throw new NMEAParseException(rawMessage, "Checksum separator expected to be asterisk(*)");

        var parameterString = msg[0];
        String checkSum = msg[1];
        String[] parameters = parameterString.split(",");
        parameterMap = new EnumMap<>(TAGBlockParameterCodeType.class);
        preFillParametersMap(parameters);
        this.timestamp = parameterMap.containsKey(TAGBlockParameterCodeType.C)
                ? Long.valueOf(parameterMap.get(TAGBlockParameterCodeType.C).getValue()) : null;
        this.destinationId = parameterMap.containsKey(TAGBlockParameterCodeType.D)
                ? parameterMap.get(TAGBlockParameterCodeType.D).getValue() : null;
        this.sentenceGrouping = parameterMap.containsKey(TAGBlockParameterCodeType.G)
                ? parameterMap.get(TAGBlockParameterCodeType.G).getValue() : null;
        this.lineCount = parameterMap.containsKey(TAGBlockParameterCodeType.N)
                ? Integer.valueOf(parameterMap.get(TAGBlockParameterCodeType.N).getValue()) : null;
        this.relativeTime = parameterMap.containsKey(TAGBlockParameterCodeType.R)
                ? Long.valueOf(parameterMap.get(TAGBlockParameterCodeType.R).getValue()) : null;
        this.sourceId = parameterMap.containsKey(TAGBlockParameterCodeType.S)
                ? parameterMap.get(TAGBlockParameterCodeType.S).getValue() : null;
        this.text = parameterMap.containsKey(TAGBlockParameterCodeType.T)
                ? parameterMap.get(TAGBlockParameterCodeType.T).getValue() : null;

        this.checksum = isBlank(checkSum) ? null : Integer.valueOf(checkSum, 16);
        this.rawMessage = rawMessage;
        this.valid = validate(parameterString);
        if (!Boolean.TRUE.equals(valid))
            throw new InvalidTagBlock(rawMessage);
    }

    private void preFillParametersMap(String[] parameters){
        for (String parameter : parameters) {
            String[] code = parameter.split(":");
            if (code.length != 2)
                throw new NMEAParseException(rawMessage, "Parameter code and its value has to be separator by colon(:)");
            if (TAGBlockParameterCodeType.contains(code[0])) {
                var nmeaTagBlockParameterCode = NMEATagBlockParameterCode.fromString(TAGBlockParameterCodeType.valueOf(code[0]), code[1]);
                parameterMap.put(nmeaTagBlockParameterCode.getCode() , nmeaTagBlockParameterCode);
            }
        }
    }

    private Boolean validate(String checkString) {
        var checksumValue = 0;
        for (var i = 0; i < checkString.length(); i++) {
            checksumValue ^= (byte)(checkString.charAt(i));
        }

        return checksumValue == this.checksum;
    }

    private static final boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    @Override
    public String toString() {
        var builder = new StringBuilder();
        List<String> parameters = new ArrayList<>();
        builder.append(" TagBlock").append("{");
        if (timestamp != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.C.getName(), timestamp));
        if (destinationId != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.D.getName(), destinationId));
        if (sentenceGrouping != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.G.getName(), sentenceGrouping));
        if (lineCount != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.N.getName(), lineCount));
        if (relativeTime != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.R.getName(), relativeTime));
        if (sourceId != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.S.getName(), sourceId));
        if (text != null)
            parameters.add(String.format(StringTools.SIMPLE_FORMATER, TAGBlockParameterCodeType.T.getName(), text));
        builder.append(String.join(",", parameters)).append(" }");
        return builder.toString();
    }
}