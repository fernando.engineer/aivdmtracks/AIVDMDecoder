/*
 * GNSSBinaryBroadcastMessage
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

public class GNSSBinaryBroadcastMessage extends AISMessage {

    private transient Integer spare1;
    private transient Float latitude;
    private transient Float longitude;
    private transient Integer spare2;
    private transient Integer mType;
    private transient Integer stationId;
    private transient Integer zCount;
    private transient Integer sequenceNumber;
    private transient Integer numOfWords;
    private transient Integer health;
    private transient String binaryData;

    public GNSSBinaryBroadcastMessage(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public GNSSBinaryBroadcastMessage(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GNSSBinaryBroadcastMessage)) return false;
        if (!super.equals(o)) return false;
        GNSSBinaryBroadcastMessage that = (GNSSBinaryBroadcastMessage) o;
        return getSpare1().equals(that.getSpare1()) && getLatitude().equals(that.getLatitude()) && getLongitude().equals(that.getLongitude())
                && getSpare2().equals(that.getSpare2()) && mType.equals(that.mType) && getStationId().equals(that.getStationId())
                && zCount.equals(that.zCount) && getSequenceNumber().equals(that.getSequenceNumber()) && getNumOfWords().equals(that.getNumOfWords())
                && getHealth().equals(that.getHealth()) && getBinaryData().equals(that.getBinaryData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSpare1(), getLatitude(), getLongitude(), getSpare2(), mType, getStationId(), zCount, getSequenceNumber(),
                getNumOfWords(), getHealth(), getBinaryData());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.GNSS_BINARY_BROADCAST_MESSAGE;
    }

	public Integer getSpare1() {
        return getDecodedValue(() -> spare1, value -> spare1 = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

	public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(58, 75)) / 10f);
	}

	public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(40, 58)) / 10f);
	}

	public Integer getSpare2() {
        return getDecodedValue(() -> spare2, value -> spare2 = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(75, 80)));
	}

	public Integer getMType() {
        return getDecodedValue(() -> mType, value -> mType = value, () -> getNumberOfBits() > 80,
                () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(80, 86)));
	}

	public Integer getStationId() {
        return getDecodedValue(() -> stationId, value -> stationId = value,
                () -> getNumberOfBits() > 80, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(86, 96)));
	}

	public Integer getZCount() {
        return getDecodedValue(() -> zCount, value -> zCount = value,
                () -> getNumberOfBits() > 80, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(96, 109)));
	}

	public Integer getSequenceNumber() {
        return getDecodedValue(() -> sequenceNumber, value -> sequenceNumber = value,
                () -> getNumberOfBits() > 80, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(109, 112)));
	}

	public Integer getNumOfWords() {
        return getDecodedValue(() -> numOfWords, value -> numOfWords = value,
                () -> getNumberOfBits() > 80, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(112, 117)));
	}

	public Integer getHealth() {
        return getDecodedValue(() -> health, value -> health = value,
                () -> getNumberOfBits() > 80, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(117, 120)));
	}

	public String getBinaryData() {
        return getDecodedValue(() -> binaryData, value -> binaryData = value, () -> getNumberOfBits() > 80, () -> BIT_DECODER.apply(getBits(80, getNumberOfBits())));
	}

    @Override
    public String toString() {
        return "GNSSBinaryBroadcastMessage{" +
                "messageType=" + getMessageType() +
                ", spare1=" + getSpare1() +
                ", latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", spare2=" + getSpare2() +
                ", mType=" + getMType() +
                ", stationId=" + getStationId() +
                ", zCount=" + getZCount() +
                ", sequenceNumber=" + getSequenceNumber() +
                ", numOfWords=" + getNumOfWords() +
                ", health=" + getHealth() +
                ", binaryData='" + getBinaryData() + '\'' +
                "} " + super.toString();
    }

}