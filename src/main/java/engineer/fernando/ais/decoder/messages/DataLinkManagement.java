/*
 * DataLinkManagement
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;



public class DataLinkManagement extends AISMessage {

    private transient Integer offsetNumber1;
    private transient Integer reservedSlots1;
    private transient Integer timeout1;
    private transient Integer increment1;
    private transient Integer offsetNumber2;
    private transient Integer reservedSlots2;
    private transient Integer timeout2;
    private transient Integer increment2;
    private transient Integer offsetNumber3;
    private transient Integer reservedSlots3;
    private transient Integer timeout3;
    private transient Integer increment3;
    private transient Integer offsetNumber4;
    private transient Integer reservedSlots4;
    private transient Integer timeout4;
    private transient Integer increment4;

    public DataLinkManagement(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public DataLinkManagement(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataLinkManagement)) return false;
        if (!super.equals(o)) return false;
        DataLinkManagement that = (DataLinkManagement) o;
        return getOffsetNumber1().equals(that.getOffsetNumber1()) && getReservedSlots1().equals(that.getReservedSlots1())
                && getTimeout1().equals(that.getTimeout1()) && getIncrement1().equals(that.getIncrement1())
                && getOffsetNumber2().equals(that.getOffsetNumber2()) && getReservedSlots2().equals(that.getReservedSlots2())
                && getTimeout2().equals(that.getTimeout2()) && getIncrement2().equals(that.getIncrement2())
                && getOffsetNumber3().equals(that.getOffsetNumber3()) && getReservedSlots3().equals(that.getReservedSlots3())
                && getTimeout3().equals(that.getTimeout3()) && getIncrement3().equals(that.getIncrement3())
                && getOffsetNumber4().equals(that.getOffsetNumber4()) && getReservedSlots4().equals(that.getReservedSlots4())
                && getTimeout4().equals(that.getTimeout4()) && getIncrement4().equals(that.getIncrement4());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getOffsetNumber1(), getReservedSlots1(), getTimeout1(), getIncrement1(), getOffsetNumber2(),
                getReservedSlots2(), getTimeout2(), getIncrement2(), getOffsetNumber3(), getReservedSlots3(), getTimeout3(), getIncrement3(),
                getOffsetNumber4(), getReservedSlots4(), getTimeout4(), getIncrement4());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.DATA_LINK_MANAGEMENT;
    }

	public Integer getOffsetNumber1() {
        return getDecodedValue(() -> offsetNumber1, value -> offsetNumber1 = value, () -> Boolean.TRUE,
                () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 52)));
	}

	public Integer getReservedSlots1() {
        return getDecodedValue(() -> reservedSlots1, value -> reservedSlots1 = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(52, 56)));
	}

	public Integer getTimeout1() {
        return getDecodedValue(() -> timeout1, value -> timeout1 = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(56, 59)));
	}

	public Integer getIncrement1() {
        return getDecodedValue(() -> increment1, value -> increment1 = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(59, 70)));
	}

	public Integer getOffsetNumber2() {
        return getDecodedValue(() -> offsetNumber2, value -> offsetNumber2 = value,
                () -> getNumberOfBits() >= 100, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(70, 82)));
	}

	public Integer getReservedSlots2() {
        return getDecodedValue(() -> reservedSlots2, value -> reservedSlots2 = value,
                () -> getNumberOfBits() >= 100, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(82, 86)));
	}

	public Integer getTimeout2() {
        return getDecodedValue(() -> timeout2, value -> timeout2 = value, () -> getNumberOfBits() >= 100,
                () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(86, 89)));
	}

	public Integer getIncrement2() {
        return getDecodedValue(() -> increment2, value -> increment2 = value,
                () -> getNumberOfBits() >= 100, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(89, 100)));
	}

	public Integer getOffsetNumber3() {
        return getDecodedValue(() -> offsetNumber3, value -> offsetNumber3 = value,
                () -> getNumberOfBits() >= 130, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(100, 112)));
	}

	public Integer getReservedSlots3() {
        return getDecodedValue(() -> reservedSlots3, value -> reservedSlots3 = value,
                () -> getNumberOfBits() >= 130, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(112, 116)));
	}

	public Integer getTimeout3() {
        return getDecodedValue(() -> timeout3, value -> timeout3 = value,
                () -> getNumberOfBits() >= 130, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(116, 119)));
	}

	public Integer getIncrement3() {
        return getDecodedValue(() -> increment3, value -> increment3 = value,
                () -> getNumberOfBits() >= 130, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(119, 130)));
	}

	public Integer getOffsetNumber4() {
        return getDecodedValue(() -> offsetNumber4, value -> offsetNumber4 = value,
                () -> getNumberOfBits() >= 160, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(130, 142)));
	}

	public Integer getReservedSlots4() {
        return getDecodedValue(() -> reservedSlots4, value -> reservedSlots4 = value,
                () -> getNumberOfBits() >= 160, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(142, 146)));
	}

	public Integer getTimeout4() {
        return getDecodedValue(() -> timeout4, value -> timeout4 = value,
                () -> getNumberOfBits() >= 160, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(146, 149)));
	}

	public Integer getIncrement4() {
        return getDecodedValue(() -> increment4, value -> increment4 = value,
                () -> getNumberOfBits() >= 160, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(149, 160)));
	}

    @Override
    public String toString() {
        return "DataLinkManagement{" +
                "messageType=" + getMessageType() +
                ", offsetNumber1=" + getOffsetNumber1() +
                ", reservedSlots1=" + getReservedSlots1() +
                ", timeout1=" + getTimeout1() +
                ", increment1=" + getIncrement1() +
                ", offsetNumber2=" + getOffsetNumber2() +
                ", reservedSlots2=" + getReservedSlots2() +
                ", timeout2=" + getTimeout2() +
                ", increment2=" + getIncrement2() +
                ", offsetNumber3=" + getOffsetNumber3() +
                ", reservedSlots3=" + getReservedSlots3() +
                ", timeout3=" + getTimeout3() +
                ", increment3=" + getIncrement3() +
                ", offsetNumber4=" + getOffsetNumber4() +
                ", reservedSlots4=" + getReservedSlots4() +
                ", timeout4=" + getTimeout4() +
                ", increment4=" + getIncrement4() +
                "} " + super.toString();
    }

}