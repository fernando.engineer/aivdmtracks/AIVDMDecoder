/*
 * SyncState
 */

package engineer.fernando.ais.decoder.messages.types;

public enum SyncState {
	UTC_DIRECT(0),
	UTC_INDIRECT(1),
	BASE_DIRECT(2),
	BASE_INDIRECT(3);

	SyncState(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static SyncState fromInteger(Integer integer) {
		if (integer != null) {
			for (SyncState b : SyncState.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}