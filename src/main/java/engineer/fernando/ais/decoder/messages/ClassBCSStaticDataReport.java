/*
 * ClassBCSStaticDataReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.messages.types.ShipType;
import engineer.fernando.ais.decoder.messages.types.TransponderClass;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.STRING_DECODER;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;

@SuppressWarnings("serial")
public class ClassBCSStaticDataReport extends AISMessage implements StaticDataReport {

    private transient Integer partNumber;
    private transient String shipName;
    private transient ShipType shipType;
    private transient String vendorId;
    private transient String callsign;
    private transient Integer toBow;
    private transient Integer toStern;
    private transient Integer toStarboard;
    private transient Integer toPort;
    private transient MMSI mothershipMmsi;

    public ClassBCSStaticDataReport(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public ClassBCSStaticDataReport(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClassBCSStaticDataReport)) return false;
        if (!super.equals(o)) return false;
        ClassBCSStaticDataReport that = (ClassBCSStaticDataReport) o;
        return getPartNumber().equals(that.getPartNumber()) && getShipName().equals(that.getShipName()) && getShipType() == that.getShipType()
                && getVendorId().equals(that.getVendorId()) && getCallsign().equals(that.getCallsign()) && getToBow().equals(that.getToBow())
                && getToStern().equals(that.getToStern()) && getToStarboard().equals(that.getToStarboard()) && getToPort().equals(that.getToPort())
                && getMothershipMmsi().equals(that.getMothershipMmsi());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPartNumber(), getShipName(), getShipType(), getVendorId(), getCallsign(), getToBow(),
                getToStern(), getToStarboard(), getToPort(), getMothershipMmsi());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.CLASS_BCS_STATIC_DATA_REPORT;
    }

    @Override
    public TransponderClass getTransponderClass() {
        return TransponderClass.B;
    }

	public Integer getPartNumber() {
        return getDecodedValue(() -> partNumber, value -> partNumber = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

	public String getShipName() {
        return getDecodedValue(() -> shipName, value -> shipName = value,
                () -> getPartNumber() == 0, () -> STRING_DECODER.apply(getBits(40, 160)));
	}

	public ShipType getShipType() {
        return getDecodedValue(() -> shipType, value -> shipType = value,
                () -> getPartNumber() == 1, () -> ShipType.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 48))));
	}

	public String getVendorId() {
        return getDecodedValue(() -> vendorId, value -> vendorId = value, () -> getPartNumber() == 1, () -> STRING_DECODER.apply(getBits(48, 90)));
	}

	public String getCallsign() {
        return getDecodedValue(() -> callsign, value -> callsign = value, () -> getPartNumber() == 1, () -> STRING_DECODER.apply(getBits(90, 132)));
	}

	public Integer getToBow() {
        return getDecodedValue(() -> toBow, value -> toBow = value,
                () -> getPartNumber() == 1, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(132, 141)));
	}

	public Integer getToStern() {
        return getDecodedValue(() -> toStern, value -> toStern = value,
                () -> getPartNumber() == 1, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(141, 150)));
	}

	public Integer getToStarboard() {
        return getDecodedValue(() -> toStarboard, value -> toStarboard = value,
                () -> getPartNumber() == 1, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(156, 162)));
	}

	public Integer getToPort() {
        return getDecodedValue(() -> toPort, value -> toPort = value, () -> getPartNumber() == 1,
                () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(150, 156)));
	}

	public MMSI getMothershipMmsi() {
        return getDecodedValue(() -> mothershipMmsi, value -> mothershipMmsi = value,
                () -> getPartNumber() == 1, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(132, 162))));
	}

    @Override
    public String toString() {
        return "ClassBCSStaticDataReport{" +
                "messageType=" + getMessageType() +
                ", partNumber=" + getPartNumber() +
                ", shipName='" + getShipName() + '\'' +
                ", shipType=" + getShipType() +
                ", vendorId='" + getVendorId() + '\'' +
                ", callsign='" + getCallsign() + '\'' +
                ", toBow=" + getToBow() +
                ", toStern=" + getToStern() +
                ", toStarboard=" + getToStarboard() +
                ", toPort=" + getToPort() +
                ", mothershipMmsi=" + getMothershipMmsi() +
                "} " + super.toString();
    }


}