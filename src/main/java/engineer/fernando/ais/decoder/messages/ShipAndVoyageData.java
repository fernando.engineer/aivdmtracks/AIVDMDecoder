/*
 * ShipAndVoyageData
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.IMO;
import engineer.fernando.ais.decoder.messages.types.PositionFixingDevice;
import engineer.fernando.ais.decoder.messages.types.ShipType;
import engineer.fernando.ais.decoder.messages.types.TransponderClass;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.StringTools;

import static engineer.fernando.ais.decoder.util.Decoders.*;

/**
 * Message has a total of 424 bits, occupying two AIVDM sentences.
 */

public class ShipAndVoyageData extends AISMessage implements StaticDataReport {

    private transient IMO imo;
    private transient String callsign;
    private transient String shipName;
    private transient ShipType shipType;
    private transient Integer toBow;
    private transient Integer toStern;
    private transient Integer toStarboard;
    private transient Integer toPort;
    private transient PositionFixingDevice positionFixingDevice;
    private transient Integer etaMonth;
    private transient Integer etaDay;
    private transient Integer etaHour;
    private transient Integer etaMinute;
    private transient Float draught;
    private transient String destination;
    private transient Boolean dataTerminalReady;

    public  ShipAndVoyageData(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA;
    }

    @Override
    public TransponderClass getTransponderClass() {
        return TransponderClass.A;
    }

	public IMO getImo() {
        return getDecodedValue(() -> imo, value -> imo = value, () -> Boolean.TRUE, () -> IMO.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

	public String getCallsign() {
        return getDecodedValue(() -> callsign, value -> callsign = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(70, 112)));
	}

	public String getShipName() {
        return getDecodedValue(() -> shipName, value -> shipName = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(112, 232)));
	}

	public ShipType getShipType() {
        return getDecodedValue(() -> shipType, value -> shipType = value, () -> Boolean.TRUE, () -> ShipType.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(232, 240))));
	}

	public Integer getToBow() {
        return getDecodedValue(() -> toBow, value -> toBow = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(240, 249)));
	}

	public Integer getToStern() {
        return getDecodedValue(() -> toStern, value -> toStern = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(249, 258)));
	}

	public Integer getToStarboard() {
        return getDecodedValue(() -> toStarboard, value -> toStarboard = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(264, 270)));
	}

	public Integer getToPort() {
        return getDecodedValue(() -> toPort, value -> toPort = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(258, 264)));
	}

	public PositionFixingDevice getPositionFixingDevice() {
        return getDecodedValue(() -> positionFixingDevice, value -> positionFixingDevice = value, () -> Boolean.TRUE,
                () -> PositionFixingDevice.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(270, 274))));
	}

	/** @return The UTC ETA Month (1-12) 0 = not available.
     */
    public Integer getEtaMonth() {
        return getDecodedValue(() -> etaMonth, value -> etaMonth = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(274, 278)));
    }

    /** @return The UTC ETA Day (1-31) 0 = not available.
     */
    public Integer getEtaDay() {
        return getDecodedValue(() -> etaDay, value -> etaDay = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(278, 283)));
    }

    /**
     * @return The UTC ETA Hour (0-23) 24 = not available.
     */
    public Integer getEtaHour() {
        return getDecodedValue(() -> etaHour, value -> etaHour = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(283, 288)));
    }

    /** @return The UTC ETA Minute (0-59) 60 = not available.
     */
    public Integer getEtaMinute() {
        return getDecodedValue(() -> etaMinute, value -> etaMinute = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(288, 294)));
    }

	public String getEta() {
        return String.format(StringTools.DATE_SIMPLE_FORMATER, this.getEtaDay(), this.getEtaMonth(), this.getEtaHour(), this.getEtaMinute());
	}


	public Float getDraught() {
        return getDecodedValue(() -> draught, value -> draught = value, () -> Boolean.TRUE, () -> UNSIGNED_FLOAT_DECODER.apply(getBits(294, 302)) / 10f);
	}

    public Integer getRawDraught() {
        return UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(294, 302));
    }

	public String getDestination() {
        return getDecodedValue(() -> destination, value -> destination = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(302, 422)));
	}

	public Boolean getDataTerminalReady() {
        return getDecodedValue(() -> dataTerminalReady, value -> dataTerminalReady = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(422, 423)));
	}

    @Override
    public String toString() {
        return "ShipAndVoyageData{" +
                "messageType=" + getMessageType() +
                ", imo=" + getImo() +
                ", callsign='" + getCallsign() + '\'' +
                ", shipName='" + getShipName() + '\'' +
                ", shipType=" + getShipType() +
                ", toBow=" + getToBow() +
                ", toStern=" + getToStern() +
                ", toStarboard=" + getToStarboard() +
                ", toPort=" + getToPort() +
                ", positionFixingDevice=" + getPositionFixingDevice() +
                ", eta='" + getEta() + '\'' +
                ", draught=" + getDraught() +
                ", destination='" + getDestination() + '\'' +
                ", dataTerminalReady=" + getDataTerminalReady() +
                "} " + super.toString();
    }

}