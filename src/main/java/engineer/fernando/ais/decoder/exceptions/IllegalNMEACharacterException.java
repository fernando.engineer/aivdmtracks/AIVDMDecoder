package engineer.fernando.ais.decoder.exceptions;

public class IllegalNMEACharacterException extends Exception
{
	public IllegalNMEACharacterException( String str )
	{
		super(str);
	}
}