package engineer.fernando.ais.decoder.exceptions;

public class InvalidTagBlock extends RuntimeException {

    public InvalidTagBlock(String message) {
        super(message);
    }
}