package engineer.fernando.ais.decoder.util;

import org.apache.logging.log4j.Logger;

public class LoggerTools {

    private LoggerTools(){
        // Auxiliary class only static methods. Shall not be instantiated.
    }

    public static void logDebug(Logger log, String s){
        if(log.isDebugEnabled()){
            log.debug(s);
        }
    }

    public static void logInfo(Logger log, String s){
        if(log.isInfoEnabled()){
            log.info(s);
        }
    }

    public static void logError(Logger log, String s) {
        if(log.isErrorEnabled()){
            log.error(s);
        }
    }
}
