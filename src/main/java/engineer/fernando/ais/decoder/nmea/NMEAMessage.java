package engineer.fernando.ais.decoder.nmea;

import engineer.fernando.ais.decoder.exceptions.NMEAParseException;
import engineer.fernando.ais.decoder.exceptions.UnsupportedNMEAMessageType;
import engineer.fernando.ais.decoder.messages.types.NMEATagBlock;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;

/*
 * NMEAMessage
 */

public class NMEAMessage implements Serializable {

	public static NMEAMessage fromString(String nmeaString) {
		return new NMEAMessage(nmeaString);
	}

	public final boolean isValid() {
        String messageType = getMessageType();
        if(messageType == null || messageType.length() != 5) return false;
		var type = messageType.substring(2);
        return "VDM".equals(type) || "VDO".equals(type);
    }

	public String getMessageType() {
	    if(preParseMsg(0) == null){
            return null;
        }
	    return Objects.requireNonNull(preParseMsg(0)).replace("!", "");
    }

    public Integer getNumberOfFragments() {
        return preParseMsgInt(1);
	}

    public Integer getFragmentNumber() {
        return preParseMsgInt(2);
	}

    public Integer getSequenceNumber() {
        return preParseMsgInt(3);
	}

    public String getRadioChannelCode() {
        return preParseMsg(4);
	}

    public String getEncodedPayload() {
	    return preParseMsg(5);
	}

    public Integer getFillBits() {
        String[] msg1 = Objects.requireNonNull(preParseMsg(6)).split("\\*");
        return isBlank(msg1[0]) ? null : Integer.valueOf(msg1[0]);
	}

    public Integer getChecksum() {
        String[] msg1 = Objects.requireNonNull(preParseMsg(6)).split("\\*");
		return isBlank(msg1[1]) ? null : Integer.valueOf(msg1[1], 16);
	}

    public String getRawMessage() {
	    return rawMessage;
	}

    public NMEATagBlock getTagBlock() {
        return tagBlock;
    }

    private NMEAMessage(String rawMessage) {
        this.rawMessage = rawMessage;
        validate();
	}

    private String preParseMsg(int field) {
        String[] msg = rawMessage.split(",");
        return isBlank(msg[field]) ? null : msg[field];
    }

    private Integer preParseMsgInt(int field){
        if(preParseMsg(field) != null){
            return Integer.valueOf(preParseMsg(field));
        }
        return null;
    }

	private void validate() {
        parseNMEATagBlockString();

	    // !AIVDM,1,1,,B,15MvlfPOh2G?nwbEdVDsnSTR00S?,0*41
        final var nmeaMessageRegExp = "^!.*\\*[0-9A-Fa-f]{2}$";

        if(!isValid()) {
            throw new UnsupportedNMEAMessageType(Integer.valueOf(getMessageType()));
        }

        if (! rawMessage.matches(nmeaMessageRegExp))
            throw new NMEAParseException(rawMessage, "Message does not comply with regexp \"" + nmeaMessageRegExp + "\"");

        String[] msg = rawMessage.split(",");
        if (msg.length != 7)
            throw new NMEAParseException(rawMessage, "Expected 7 fields separated by commas; got " + msg.length);

        String[] msg1 = msg[6].split("\\*");
        if (msg1.length != 2)
            throw new NMEAParseException(rawMessage, "Expected checksum fields to start with *");
    }

    private void parseNMEATagBlockString() {
	    final var nmeaTagBlockRegEx = "^\\\\.*\\*[0-9A-Fa-f]{2}\\\\";

        var pattern = Pattern.compile(nmeaTagBlockRegEx);
        var matcher = pattern.matcher(rawMessage);
        if (matcher.lookingAt())  {
            var nmeaTagBlockString = rawMessage.substring(matcher.start(), matcher.end());
            this.tagBlock = NMEATagBlock.fromString(nmeaTagBlockString);
            this.rawMessage = this.rawMessage.substring(matcher.end());
        }
        else {
            this.tagBlock = null;
        }
    }

    @Override
    public String toString() {
        return "NMEAMessage{" +
                "rawMessage='" + rawMessage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NMEAMessage that = (NMEAMessage) o;

        return rawMessage.equals(that.rawMessage);
    }

    @Override
    public int hashCode() {
        return rawMessage.hashCode();
    }

    private static boolean isBlank(String s) {
		return s == null || s.trim().length() == 0;
	}

	private String rawMessage;
	private transient NMEATagBlock tagBlock;
}