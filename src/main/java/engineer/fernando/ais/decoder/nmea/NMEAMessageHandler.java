/*
 * AISMessages
 */

package engineer.fernando.ais.decoder.nmea;

import engineer.fernando.ais.decoder.messages.Metadata;
import engineer.fernando.ais.decoder.util.LoggerTools;
import engineer.fernando.ais.decoder.messages.AISMessage;
import org.apache.logging.log4j.LogManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import org.apache.logging.log4j.Logger;


/**
 * Receives NMEA messages containing AIS strings.
 * An AIS message may contains several NMEA messages.
 * When a AIS message is completed in the input,it is built and transmited.
 */
public class NMEAMessageHandler implements Consumer<NMEAMessage> {

	private static final Logger LOG = LogManager.getLogger(NMEAMessageHandler.class);

	private final String source;
    private final ArrayList<NMEAMessage> messageFragments = new ArrayList<>();
    private final List<Consumer<? super AISMessage>> aisMessageReceivers = new LinkedList<>();

    public NMEAMessageHandler(String source, Consumer<? super AISMessage>... aisMessageReceivers) {
    	this.source = source;
        for (Consumer<? super AISMessage> aisMessageReceiver : aisMessageReceivers) {
            addAisMessageReceiver(aisMessageReceiver);
        }
    }

    /**
     * Receive a single NMEA amoured AIS string
     * @param nmeaMessage the NMEAMessage to handle.
     */
    @Override
    public void accept(NMEAMessage nmeaMessage) {
		LoggerTools.logDebug(LOG, new  StringBuffer().append("Received for processing: ")
				.append(nmeaMessage.getRawMessage()).toString());

		if (! nmeaMessage.isValid()) {
			LoggerTools.logError(LOG, new  StringBuffer().append("NMEA message is invalid: ")
					.append(nmeaMessage).toString());
			return;
		}

		int numberOfFragments = nmeaMessage.getNumberOfFragments();
		if (numberOfFragments <= 0) {
			LoggerTools.logError(LOG, new  StringBuffer().append("NMEA message is invalid: ")
					.append(nmeaMessage).toString());
			messageFragments.clear();
		} else if (numberOfFragments == 1) {
			LoggerTools.logDebug(LOG, new  StringBuffer().append("Handling unfragmented NMEA message: ").toString());
            var aisMessage = AISMessage.create(new Metadata(source), nmeaMessage.getTagBlock(), nmeaMessage);
            sendToAisMessageReceivers(aisMessage);
			messageFragments.clear();
		} else {
			handleFragmentedNMEAMessage(nmeaMessage);
		}
	}

	private void handleFragmentedNMEAMessage(NMEAMessage nmeaMessage) {
		int numberOfFragments = nmeaMessage.getNumberOfFragments();
		int fragmentNumber = nmeaMessage.getFragmentNumber();
		LoggerTools.logDebug(LOG, new  StringBuffer().append("Handling fragmented NMEA message with fragment number ")
				.append(fragmentNumber).toString());
		if (fragmentNumber < 0) {
			LoggerTools.logError(LOG, new  StringBuffer().append("Fragment number cannot be negative: ")
					.append(fragmentNumber).append( ": ").append(nmeaMessage.getRawMessage()).toString());
			messageFragments.clear();
		} else if (fragmentNumber > numberOfFragments) {
			LoggerTools.logError(LOG, new  StringBuffer().append("Fragment number ").append(fragmentNumber)
					.append(" higher than expected ").append(numberOfFragments).append(" ")
					.append(nmeaMessage.getRawMessage()).toString());
			messageFragments.clear();
		} else {
			int expectedFragmentNumber = messageFragments.size() + 1;
			LoggerTools.logError(LOG, new  StringBuffer().append("Expected fragment number is: ")
					.append(expectedFragmentNumber).append( ": ").append(nmeaMessage.getRawMessage()).toString());
			if (expectedFragmentNumber != fragmentNumber) {
				LoggerTools.logError(LOG, new  StringBuffer().append("Expected fragment number ")
						.append(expectedFragmentNumber).append( "; not ").append(nmeaMessage.getRawMessage())
						.toString());
				messageFragments.clear();
			} else {
				messageFragments.add(nmeaMessage);
				LoggerTools.logDebug(LOG, new  StringBuffer().append("NMEAMessage's number of fragments: ")
						.append(nmeaMessage.getNumberOfFragments()).toString());
				LoggerTools.logDebug(LOG, new  StringBuffer().append("NMEAMessage's fragments size: ")
						.append(messageFragments.size()).toString());
				if (nmeaMessage.getNumberOfFragments() == messageFragments.size()) {
					var aisMessage = AISMessage.create(new Metadata(source), nmeaMessage.getTagBlock(), messageFragments.toArray(new NMEAMessage[messageFragments.size()]));
					sendToAisMessageReceivers(aisMessage);
					messageFragments.clear();
				} else
					LoggerTools.logError(LOG, new  StringBuffer().append("Fragmented message not yet complete; missing ")
							.append(nmeaMessage.getNumberOfFragments() - messageFragments.size())
							.append(" fragment(s).").toString());
			}
		}
	}

	/** Send encoded AIS message to all interested receivers. */
    private void sendToAisMessageReceivers(final AISMessage aisMessage) {
        aisMessageReceivers.forEach(r -> r.accept(aisMessage));
    }

    /**
     * Add a consumer of encoded AIS messages.
     * @param aisMessageReceiver The consumer to add.
     */
    @SuppressWarnings("unused")
    public void addAisMessageReceiver(Consumer<? super AISMessage> aisMessageReceiver) {
        aisMessageReceivers.add(aisMessageReceiver);
    }

    /**
	 * Empty buffer of unhandled messages and return those not handled.
     * @return List of unhandled NMEAMessages.
	 */
	@SuppressWarnings("unchecked")
	public List<NMEAMessage> flush() {
		ArrayList<NMEAMessage> unhandled = (ArrayList<NMEAMessage>) messageFragments.clone();
		messageFragments.clear();
		return unhandled;
	}

}