package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.UTCAndDateResponse;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType11_UTCAndDateResponseTest extends TestCase {

    public void testFullParse() {
        String demoNmeaStrings = "!AIVDM,1,1,,B,;8u:8CAuiT7Bm2CIM=fsDJ100000,0*51";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.UTC_AND_DATE_RESPONSE, supermsg.getMessageType());
        assertTrue(supermsg instanceof UTCAndDateResponse);
        UTCAndDateResponse msg = (UTCAndDateResponse) supermsg;
        assertEquals("userid", 601000013, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("Year", 2012, msg.getYear().intValue());
        assertEquals("Month", 6, msg.getMonth().intValue());
        assertEquals("Day", 8, msg.getDay().intValue());
        assertEquals("Hour", 7, msg.getHour().intValue());
        assertEquals("Minute", 18, msg.getMinute().intValue());
        assertEquals("Second", 53, msg.getSecond().intValue());
        assertEquals("longitude", 32.199531, msg.getLongitude(),0.000001);
        assertEquals("latitude", -29.83748, msg.getLatitude(),0.000001);
        assertFalse(msg.getPositionAccurate());
        assertEquals("Position Fixing Device Value", "GPS", msg.getPositionFixingDevice().getValue());
        assertEquals("Position Fixing Device Code", 1, msg.getPositionFixingDevice().getCode().intValue());
        assertFalse("Raim Flag", msg.getRaimFlag());
        assertEquals("repeat", 0, msg.getRepeatIndicator().intValue());
    }
}
