package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.DataLinkManagement;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType20_DataLinkManagementTest extends TestCase {

    public void testFullParse() {
        String demoNmeaStrings = "!AIVDM,1,1,,A,Dh3Ovl0mMN>4,0*38";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.DATA_LINK_MANAGEMENT, supermsg.getMessageType());
        assertTrue(supermsg instanceof DataLinkManagement);
        DataLinkManagement msg = (DataLinkManagement) supermsg;
        assertEquals("userid", 3669712, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("OffSet Number 1", 855, msg.getOffsetNumber1().intValue());
        assertEquals("Reserved Slots 1", 5, msg.getReservedSlots1().intValue());
        assertEquals("Timeout 1", 7, msg.getTimeout1().intValue());
        assertEquals("Increment 1", 225, msg.getIncrement1().intValue());
        assertNull("OffSer Number 2", msg.getOffsetNumber2());
        assertNull("Reserved Slots 2", msg.getReservedSlots2());
        assertNull("Timeout 2", msg.getTimeout2());
        assertNull("Increment 2", msg.getIncrement2());
        assertNull("OffSer Number 3", msg.getOffsetNumber3());
        assertNull("Reserved Slots 3", msg.getReservedSlots3());
        assertNull("Timeout 3", msg.getTimeout3());
        assertNull("Increment 3", msg.getIncrement3());
        assertNull("OffSer Number 4", msg.getOffsetNumber4());
        assertNull("Reserved Slots 4", msg.getReservedSlots4());
        assertNull("Timeout 4", msg.getTimeout4());
        assertNull("Increment 4", msg.getIncrement4());
        assertEquals("repeat", 3, msg.getRepeatIndicator().intValue());
    }
}
