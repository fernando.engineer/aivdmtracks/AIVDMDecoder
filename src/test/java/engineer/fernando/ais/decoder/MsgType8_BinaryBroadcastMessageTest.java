package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.BinaryBroadcastMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType8_BinaryBroadcastMessageTest extends TestCase {

    public void testFullParse() {
        String demoNmeaStrings = "!AIVDM,1,1,,B,85MwpKiKf0wLgSt5BlHF<3FMlaSRCjf1?Nq;4TAA7Mj:oOH5bs=8,0*7D";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.BINARY_BROADCAST_MESSAGE, supermsg.getMessageType());
        assertTrue(supermsg instanceof BinaryBroadcastMessage);
        BinaryBroadcastMessage msg = (BinaryBroadcastMessage) supermsg;
        assertEquals("userid", 366999663, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("Spare", 0, msg.getSpare().intValue());
        assertEquals("designatedAreaCode", 366, msg.getDesignatedAreaCode().intValue());
        assertEquals("functionalId", 56, msg.getFunctionalId().intValue());
        String strBinaryData = "0000111111011100101111100011111100000101010010110100011000010110001100000011010110011101110100101001100011100010010011110010101110000001001111011110111001001011000100100100010001010001000111011101110010001010110111011111011000000101101010111011001101001000";
        assertEquals("binaryData", strBinaryData, msg.getBinaryData());
    }
}
