package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.SOTDMACommunicationState;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.BaseStationReport;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType4_BaseStationReportTest  extends TestCase  {
    public void testFullParse_SOTDMACommunicationState_slotTimeout0() {
        String demoNmeaStrings = "!AIVDM,1,1,,A,400TcdiuiT7VDR>3nIfr6>i00000,0*78";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.BASE_STATION_REPORT, supermsg.getMessageType());
        assertTrue(supermsg instanceof BaseStationReport);
        BaseStationReport msg = (BaseStationReport) supermsg;
        assertEquals("userid", 601011, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("repeat", 0, msg.getRepeatIndicator().intValue());
        assertEquals("Year", 2012, msg.getYear().intValue());
        assertEquals("Month", 6, msg.getMonth().intValue());
        assertEquals("Day", 8, msg.getDay().intValue());
        assertEquals("Hour", 7, msg.getHour().intValue());
        assertEquals("Minute", 38, msg.getMinute().intValue());
        assertEquals("Second", 20, msg.getSecond().intValue());
        assertTrue("Position Accurate", msg.getPositionAccurate());
        assertEquals("Longitude", 31.033514, msg.getLongitude(),0.000001);
        assertEquals("Latitude", -29.870832, msg.getLatitude(),0.000001);
        assertEquals("Position Fixing Device Value", "GPS", msg.getPositionFixingDevice().getValue());
        assertEquals("Position Fixing Device Code", 1, msg.getPositionFixingDevice().getCode().intValue());
        assertFalse("Raim Flag", msg.getRaimFlag());
        assertTrue("Communication State" , msg.getCommunicationState() instanceof SOTDMACommunicationState);
        SOTDMACommunicationState commState = (SOTDMACommunicationState) msg.getCommunicationState();
        System.out.println("Communication State details: " + commState);
        assertEquals("Slot Timeout", 0, commState.getSlotTimeout().intValue());
        assertEquals("# of Received Stations", null, commState.getNumberOfReceivedStations());
        assertEquals("Slot Number", null, commState.getSlotNumber());
        assertEquals("UTH Hour", null, commState.getUtcHour());
        assertEquals("UTH Minute", null, commState.getUtcMinute());
        assertEquals("Slot OffSet", 0, commState.getSlotOffset().intValue());
    }

}
