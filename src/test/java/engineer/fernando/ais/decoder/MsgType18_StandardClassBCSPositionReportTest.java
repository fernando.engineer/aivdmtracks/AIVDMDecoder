package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.StandardClassBCSPositionReport;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.ITDMACommunicationState;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType18_StandardClassBCSPositionReportTest extends TestCase {

    public void testFullParse_ITDMACommunicationState() {
        String demoNmeaStrings = "!AIVDM,1,1,,B,B5NJ;PP005l4onUIsc@03woUoP06,0*3A";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.STANDARD_CLASS_BCS_POSITION_REPORT, supermsg.getMessageType());
        assertTrue(supermsg instanceof StandardClassBCSPositionReport);
        StandardClassBCSPositionReport msg = (StandardClassBCSPositionReport) supermsg;
        assertEquals("userid", 367430530, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("regionalReserved1", "00000000", msg.getRegionalReserved1());
        assertEquals("speedOverGround", 0.0, msg.getSpeedOverGround(),0.01);
        assertFalse(msg.getPositionAccurate());
        assertEquals("longitude", -122.267334, msg.getLongitude(),0.000001);
        assertEquals("latitude", 37.785046, msg.getLatitude(),0.000001);
        assertEquals("Curse over Ground", 0.0, msg.getCourseOverGround(), 0.01);
        assertEquals("trueHeading", 511, msg.getTrueHeading().intValue());
        assertEquals("utc_sec", 47, msg.getSecond().intValue());
        assertEquals("regionalReserved2", "00", msg.getRegionalReserved2());
        assertTrue("csUnit",msg.getCsUnit());
        assertFalse(msg.getDisplay());
        assertTrue("dsc",msg.getDsc());
        assertTrue("band",msg.getBand());
        assertTrue("message22",msg.getMessage22());
        assertFalse("message22",msg.getAssigned());
        assertTrue("Raim Flag", msg.getRaimFlag());
        assertTrue("commStateSelectorFlag", msg.getCommunicationStateSelectorFlag());
        assertTrue("Communication State" , msg.getCommunicationState() instanceof ITDMACommunicationState);
        ITDMACommunicationState commState = (ITDMACommunicationState) msg.getCommunicationState();
        assertEquals("Sync State Code", 3, commState.getSyncState().getCode().intValue());
        assertEquals("Sync State Value", "BASE_INDIRECT", commState.getSyncState().getValue());
        assertEquals("slotIncrement", 0, commState.getSlotIncrement().intValue());
        assertEquals("numberOfSlots", 3, commState.getNumberOfSlots().intValue());
        assertFalse("keepFlag", commState.getKeepFlag());
        assertEquals("repeat", 0, msg.getRepeatIndicator().intValue());
    }
}
